// swift-tools-version:5.6
import PackageDescription

let package = Package(
    name: "BssBaseKit",
    platforms: [
        .iOS(.v9),
    ],
    products: [
        .library(
            name: "BssBaseKit",
            targets: [
                "BssBaseKit",
            ]
        ),
    ],
    targets: [
        .binaryTarget(name: "BssBaseKit", path: "BssBaseKit/Frameworks/BssBaseKit.xcframework")
    ]
)
