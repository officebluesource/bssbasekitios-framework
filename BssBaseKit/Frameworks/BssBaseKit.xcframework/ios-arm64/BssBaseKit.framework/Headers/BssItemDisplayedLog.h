//
//  BssItemDisplayedLog.h
//  BssBaseKit
//
//  Created by Manya Egon on 24.05.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import "BssLog.h"

@interface BssItemDisplayedLog : BssLog

@property (strong, nonatomic, nonnull) NSNumber * itemDisplayedListIndex;
@property (strong, nonatomic, nonnull) NSString * itemDisplayedScreenPosition;

@end
