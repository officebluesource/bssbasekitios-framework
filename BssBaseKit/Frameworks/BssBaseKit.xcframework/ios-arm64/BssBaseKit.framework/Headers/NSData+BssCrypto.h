//
//  NSString+Crypto.h
//  app
//
//  Created by Florian Hager on 13.02.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (BssCrypto)

- (NSData *)BSS_AES256EncryptWithKey:(NSString *)key;
- (NSData *)BSS_AES256DecryptWithKey:(NSString *)key;

@end
