//
//  BssVoucherType.h
//  BssBaseKit
//
//  Created by Hager Florian on 11.11.22.
//  Copyright © 2022 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    BssRedemptionTypeUnknown,
    BssRedemptionTypeCode,
    BssRedemptionTypeBarcode,
    BssRedemptionTypeImage,
    BssRedemptionTypePhone,
    BssRedemptionTypeWebManual,
    BssRedemptionTypeWebAuto,
    BssRedemptionTypeWebNoCode,
    BssRedemptionTypePoints,
    BssRedemptionTypeLottoParticipation
} BssRedemptionTypeEnum;

@interface BssRedemptionType : NSObject

+ (BssRedemptionTypeEnum)convertToEnum:(NSString * _Nullable)type;

@end

NS_ASSUME_NONNULL_END
