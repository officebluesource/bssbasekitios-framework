//
//  BssInsuranceType.h
//  BssBaseKit
//
//  Created by Hager Florian on 06.07.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssCatalogElementType.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssInsuranceType : BssCatalogElementType

@property (strong, nonatomic, nullable) NSString <Optional> * reportUrl;
@property (strong, nonatomic, nullable) NSString <Optional> * serviceHotline;

@end

NS_ASSUME_NONNULL_END
