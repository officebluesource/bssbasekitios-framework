//
//  BssItemOrderDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 30.08.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BssItemOrder : NSObject

+ (nonnull NSString *)defaultOrder;
+ (nonnull NSString *)redeemablePoints;

@end
