//
//  BssItemVoucherResultDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssRedemptionType.h"

@interface BssRedeemedCouponResponse : BssJSONModel

@property (strong, nonatomic, nullable) NSDate <Optional> * timestamp;

@property (strong, nonatomic, nullable) NSString <Optional> * couponType;
@property (strong, nonatomic, nullable) NSString <Optional> * descr;
@property (strong, nonatomic, nullable) NSString <Optional> * customCode; // type = CODE || PHONE || WEB_MANUAL || WEB_AUTO
@property (strong, nonatomic, nullable) NSString <Optional> * barcodeNumber; // type = BARCODE
@property (strong, nonatomic, nullable) NSString <Optional> * barcodeType; // type = BARCODE
@property (strong, nonatomic, nullable) NSString <Optional> * base64EncodedImage; // type = BARCODE || IMAGE
@property (strong, nonatomic, nullable) NSString <Optional> * phoneNumber; // type = PHONE
@property (strong, nonatomic, nullable) NSString <Optional> * url; // type = WEB_MANUAL || WEB_AUTO || WEB_NO_CODE

- (BssRedemptionTypeEnum)couponTypeEnum;

@end
