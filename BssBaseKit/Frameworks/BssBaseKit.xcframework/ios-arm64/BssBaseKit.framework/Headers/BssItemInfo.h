//
//  BssItemInfo.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssItem.h"

@interface BssItemInfo : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * itemId;
@property (strong, nonatomic, nonnull) NSDate * creationTimestamp;
@property (strong, nonatomic, nonnull) NSString * type;

- (BssItemType)typeEnum;

@end
