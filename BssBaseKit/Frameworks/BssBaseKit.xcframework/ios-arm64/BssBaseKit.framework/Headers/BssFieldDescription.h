//
//  BssFieldDescription.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssFieldDescription : BssJSONModel

@property (assign) BOOL required;
@property (strong, nonatomic, nonnull) NSString * type;

- (BOOL)isTypeTitle;
- (BOOL)isTypeFirstName;
- (BOOL)isTypeLastName;
- (BOOL)isTypeCompany;
- (BOOL)isTypeStreet1;
- (BOOL)isTypeStreet2;
- (BOOL)isTypeHousenumber;
- (BOOL)isTypeZip;
- (BOOL)isTypeCity;
- (BOOL)isTypeCountry;
- (BOOL)isTypeEmail;
- (BOOL)isTypePhone;
- (BOOL)isTypeMobilePhone;
- (BOOL)isTypeFax;
- (BOOL)isTypeBirthday;
- (BOOL)isTypeGender;
- (BOOL)isTypeRetailerStore;
- (BOOL)isTypeUsername;
- (BOOL)isTypePassword;

@end
