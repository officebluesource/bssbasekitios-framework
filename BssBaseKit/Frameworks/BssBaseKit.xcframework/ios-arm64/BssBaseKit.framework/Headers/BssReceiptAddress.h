//
//  BssPSACreationAddress.h
//  BssPSALoyalty
//
//  Created by Viacheslav Korkhonen on 08.03.19.
//  Copyright © 2019 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssReceiptAddress : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * city;
@property (strong, nonatomic, nullable) NSString <Optional> * countryCode;
@property (strong, nonatomic, nullable) NSString <Optional> * postCode;
@property (strong, nonatomic, nullable) NSString <Optional> * streetAddress;
@property (strong, nonatomic, nullable) NSString <Ignore> * uniqueId;

@end

NS_ASSUME_NONNULL_END
