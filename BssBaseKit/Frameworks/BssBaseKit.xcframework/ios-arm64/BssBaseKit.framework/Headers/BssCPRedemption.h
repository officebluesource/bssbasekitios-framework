//
//  BssCPRedemption.h
//  BssBaseKit
//
//  Created by Baumgartner Mario on 29.06.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssCPRedemption : BssJSONModel

@property (strong, nonatomic, nullable) NSNumber <Optional> * redeemablePoints;
@property (strong, nonatomic, nullable) NSNumber <Optional> * redemptionsPerUser;

@end

NS_ASSUME_NONNULL_END
