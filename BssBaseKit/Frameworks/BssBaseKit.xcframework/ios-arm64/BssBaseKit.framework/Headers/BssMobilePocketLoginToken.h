//
//  BssMobilePocketLoginToken.h
//  BssBaseKit
//
//  Created by Hager Florian on 06.06.19.
//  Copyright © 2019 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssMobilePocketLoginToken : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * loginToken;

@end

NS_ASSUME_NONNULL_END
