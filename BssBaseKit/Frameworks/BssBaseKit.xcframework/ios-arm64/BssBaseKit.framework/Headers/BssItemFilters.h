//
//  BssItemFiltersDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 30.08.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssItem.h"

@interface BssItemFilters : NSObject

@property (assign) BOOL onlyUnrestrictedItems;

- (nonnull NSString *)toString;

- (void)reset;

- (void)addCardWithCardTypId:(nonnull NSString *)cardTypeId cardId:(nonnull NSString *)cardId;
- (void)addItemType:(BssItemType)itemType;
- (void)addTagWithRoot:(nonnull NSString *)rootTag childTags:(nullable NSArray<NSString *> *)childTags;
- (void)addCountryIsoCode:(nonnull NSString *)isoCode;

@end
