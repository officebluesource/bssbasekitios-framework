//
//  BssUpdateInsurance.h
//  BssBaseKit
//
//  Created by Baumgartner Mario on 10.08.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssImage.h"
#import "BssContactPerson.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssUpdateInsurance : BssJSONModel

@property (strong, nonatomic, nullable) NSString * customName;
@property (strong, nonatomic, nullable) NSString * notes;
@property (strong, nonatomic, nullable) BssImage <Optional>* customBackImage;
@property (strong, nonatomic, nonnull) NSString * insuranceKind;
@property (strong, nonatomic, nullable) NSString * policyNumber;
@property (strong, nonatomic, nullable) NSString * customServiceHotline;
@property (strong, nonatomic, nullable) BssContactPerson <Optional> * contactPerson;

- (instancetype)initWithCustomName:(NSString * _Nullable)customName
                            notes:(NSString * _Nullable)notes
                   customBackImage:(BssImage * _Nullable)customBackImage
                     insuranceKind:(NSString * _Nonnull)insuranceKind
                      policyNumber:(NSString * _Nullable)policyNumber
             customServiceHotline:(NSString * _Nullable)customServiceHotline
                     contactPerson:(BssContactPerson * _Nullable)contactPerson;

@end

NS_ASSUME_NONNULL_END
