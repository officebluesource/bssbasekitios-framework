//
//  BssInsuranceKind.h
//  BssBaseKit
//
//  Created by Hager Florian on 12.07.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, BssInsuranceKindEnum) {
    BssInsuranceKindCar,
    BssInsuranceKindMotorbike,
    BssInsuranceKindHousehold,
    BssInsuranceKindHome,
    BssInsuranceKindTravel,
    BssInsuranceKindLegalProtection,
    BssInsuranceKindLife,
    BssInsuranceKindAccident,
    BssInsuranceKindPersonalLiability,
    BssInsuranceKindDog,
    BssInsuranceKindCat,
    BssInsuranceKindEbike,
    BssInsuranceKindBike,
    BssInsuranceKindHealth,
    BssInsuranceKindOthers,
    BssInsuranceKindUnknown
};

@interface BssInsuranceKind : NSObject

+ (BssInsuranceKindEnum)convertToEnum:(NSString * _Nullable)kind;
+ (NSString *)convertToString:(BssInsuranceKindEnum)kind;

@end

NS_ASSUME_NONNULL_END
