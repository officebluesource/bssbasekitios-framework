//
//  BssCurrenciesInfoModel.h
//  BssBaseKit
//
//  Created by Korkhonen Viacheslav on 04.02.22.
//  Copyright © 2022 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssCurrenciesContributions.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssCurrenciesInfoModel : BssJSONModel
@property (nonatomic, strong, nonnull) BssCurrenciesContributions * currencies;
@end

NS_ASSUME_NONNULL_END
