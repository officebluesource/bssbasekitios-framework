//
//  BssAlertController.h
//  BssBaseKit
//
//  Created by Florian Hager on 04.09.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssBase.h"

@interface BssAlertController : NSObject

// display the returning webservice error in a system popup. This method automatically handles timeouts or internet connection errors.
+ (void)handleWebserviceError:(NSError * _Nonnull)error dismissAction:(BssEmptyBlock _Nullable)dismissedBlock;

// show predefined error messages in a system popup.
+ (void)showServerMaintenanceErrorWithDismissAction:(_Nullable  BssEmptyBlock)dismissedBlock;
+ (void)showInternetConnectionErrorWithDismissAction:(_Nullable  BssEmptyBlock)dismissedBlock;

// show system popup with canel button and max one action button.
+ (void)showAlertWithTitle:(NSString * _Nullable)title message:(NSString * _Nullable)message dismissed:(_Nullable BssEmptyBlock)dismissedBlock;
+ (void)showAlertWithTitle:(NSString * _Nullable)title message:(NSString * _Nullable)message tintColor:(UIColor * _Nullable )tintColor dismissed:(_Nullable BssEmptyBlock)dismissedBlock;
+ (void)showAlertWithTitle:(NSString * _Nullable)title message:(NSString * _Nullable)message actionButtonTitle:(NSString * _Nullable)actionButtonTitle destructive:(BOOL)destructive action:(BssEmptyBlock _Nullable)actionBlock dismissButtonTitle:(NSString * _Nullable)dismissButtonTitle dismissed:(_Nullable BssEmptyBlock)dismissedBlock;
+ (void)showAlertWithTitle:(NSString * _Nullable)title message:(NSString * _Nullable)message tintColor:(UIColor * _Nullable)tintColor actionButtonTitle:(NSString * _Nullable)actionButtonTitle destructive:(BOOL)destructive action:(_Nullable BssEmptyBlock)actionBlock dismissButtonTitle:(NSString * _Nullable)dismissButtonTitle dismissed:(_Nullable BssEmptyBlock)dismissedBlock;
// show system popup with textfield, cancel button and max two action buttons
+ (void)showAlertWithTextfield:(UITextField * _Nonnull)textfield title:(NSString * _Nullable)title message:(NSString * _Nullable)message actionButtonTitle:(NSString * _Nullable)actionButtonTitle destructive:(BOOL)destructive action:(void (^_Nullable)(NSString * _Nullable text))actionBlock action2ButtonTitle:(NSString * _Nullable)action2ButtonTitle action2:(_Nullable BssEmptyBlock)action2Block dismissButtonTitle:(NSString * _Nullable)dismissButtonTitle dismissed:(_Nullable BssEmptyBlock)dismissedBlock;
+ (void)showAlertWithTextfield:(UITextField * _Nonnull)textfield title:(NSString * _Nullable)title message:(NSString * _Nullable)message tintColor:(UIColor * _Nullable)tintColor actionButtonTitle:(NSString * _Nullable)actionButtonTitle destructive:(BOOL)destructive action:(void (^_Nullable)(NSString * _Nullable text))actionBlock action2ButtonTitle:(NSString * _Nullable)action2ButtonTitle action2:(_Nullable BssEmptyBlock)action2Block dismissButtonTitle:(NSString * _Nullable)dismissButtonTitle dismissed:(_Nullable BssEmptyBlock)dismissedBlock;

// displays a system popup with no buttons for a defined duration.
+ (void)showToastWithMesssage:(NSString * _Nullable)message duration:(float)duration finished:(_Nullable BssEmptyBlock)finished;

@end
