//
//  BssCPCardType.h
//  BssBaseKit
//
//  Created by Baumgartner Mario on 28.06.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssCPGoodiePreview.h"
#import "BssJSONModel.h"
#import "BssCPRetailer.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BssCPGoodiePreview;

@interface BssCPCardType : BssJSONModel

@property (nonatomic, strong, nonnull) NSString * uniqueId;
@property (nonatomic, strong, nonnull) NSString * publicId;
@property (nonatomic, strong, nonnull) BssCPRetailer * retailer;
@property (nonatomic, strong, nullable) NSArray<BssCPGoodiePreview *> <BssCPGoodiePreview, Optional> * items;

@end

NS_ASSUME_NONNULL_END
