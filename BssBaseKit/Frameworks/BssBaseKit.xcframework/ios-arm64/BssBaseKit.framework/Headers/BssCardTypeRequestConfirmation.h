//
//  BssCardTypeRequestConfirmationDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 17.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssCardTypeRequestConfirmation : BssJSONModel

@property (nonatomic, strong, nullable) NSString <Optional> * requestToken;
@property (nonatomic, strong, nullable) NSString <Optional> * title;
@property (nonatomic, strong, nullable) NSString <Optional> * firstName;
@property (nonatomic, strong, nullable) NSString <Optional> * lastName;
@property (nonatomic, strong, nullable) NSString <Optional> * company;
@property (nonatomic, strong, nullable) NSString <Optional> * street1;
@property (nonatomic, strong, nullable) NSString <Optional> * street2;
@property (nonatomic, strong, nullable) NSString <Optional> * houseNumber;
@property (nonatomic, strong, nullable) NSString <Optional> * zip;
@property (nonatomic, strong, nullable) NSString <Optional> * city;
@property (nonatomic, strong, nullable) NSString <Optional> * countryIsoCode;
@property (nonatomic, strong, nullable) NSString <Optional> * email;
@property (nonatomic, strong, nullable) NSString <Optional> * phoneNumber;
@property (nonatomic, strong, nullable) NSString <Optional> * mobilePhoneNumber;
@property (nonatomic, strong, nullable) NSString <Optional> * fax;
@property (nonatomic, strong, nullable) NSString <Optional> * username;
@property (nonatomic, strong, nullable) NSString <Optional> * password;
@property (nonatomic, strong, nullable) NSString <Optional> * birthday;
@property (nonatomic, strong, nullable) NSString <Optional> * gender;
@property (nonatomic, strong, nullable) NSString <Optional> * otherGenderDetails;
@property (nonatomic, strong, nullable) NSString <Optional> * retailerStoreId;
@property (nonatomic, assign) BOOL marketingAgreement;

@property (nonatomic, assign) BOOL persistPersonalData;

- (BssCardTypeRequestConfirmation * _Nonnull)copyObject;

@end
