//
//  BssCardType.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssCatalogElementType.h"
#import "BssSecurityExtensionDescription.h"
#import "BssColorScheme.h"
#import "BssTag.h"

@protocol BssTag;

@interface BssCardType : BssCatalogElementType

@property (nonatomic) BOOL active;
@property (nonatomic) BOOL backImageEditable;
@property (nonatomic) BOOL backImageMandatory;
@property (nonatomic) BOOL barcodeNumberEditable;
@property (nonatomic) BOOL barcodeNumberMandatory;
@property (nonatomic) BOOL barcodeTypeEditable;
@property (nonatomic) BOOL bluecodeEnabled;
@property (nonatomic) BOOL cardNumberEditable;
@property (nonatomic) BOOL cardNumberMandatory;
@property (nonatomic) BOOL deletable;
@property (nonatomic) BOOL frontImageEditable;
@property (nonatomic) BOOL frontImageMandatory;
@property (nonatomic) BOOL hasBonusProgram;
@property (nonatomic) BOOL nameEditable;
@property (nonatomic) BOOL requestable;
@property (nonatomic) BOOL creatable;
@property (nonatomic) BOOL uniqueCardTypeAlreadyCreated;

@property (nonatomic, strong, nullable) NSString <Optional> * barcodeType;
@property (nonatomic, strong, nullable) NSString <Optional> * creationHint;
@property (nonatomic, strong, nonnull) NSString * cardNumberVisibility;
@property (nonatomic, strong, nullable) NSNumber <Optional> * indexId DEPRECATED_ATTRIBUTE;

@property (nonatomic, strong, nullable) BssSecurityExtensionDescription <Optional> * securityExtensionDescription;
@property (nonatomic, strong, nullable) BssImage <Optional> * backImage;
@property (nonatomic, strong, nullable) BssColorScheme <Optional> * colorScheme;

@property (nonatomic, strong, nullable) NSArray<BssTag *> <BssTag, Optional> * tags;

#pragma mark - Additions & Helper
- (BOOL)isOtherCard;

@end
