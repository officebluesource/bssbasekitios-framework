//
//  BssExternalId.h
//  BssBaseKit
//
//  Created by Hager Florian on 05.03.24.
//  Copyright © 2024 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssExternalId : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * key;
@property (strong, nonatomic, nonnull) NSString * value;

@end

NS_ASSUME_NONNULL_END
