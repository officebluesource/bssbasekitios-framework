//
//  BssAuthManager.h
//  BssBaseKit
//
//  Created by Florian Hager on 09.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssBase.h"
#import "BssWebserviceClient.h"
#import "BssMobilePocketLoginToken.h"
#import "BssLoginCredentials.h"


@interface BssAuthManager : NSObject

/*!
 * @discussion Use this instance for all method calls inside this class.
 */
+ (instancetype _Nonnull)sharedInstance;


#pragma mark - Login

- (BssLoginCredentials * _Nonnull)loginCredentialEntityWithType:(NSString* _Nullable)loginType;

/*!
 * @discussion Anonymous login of the user. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginAnonymousWithSuccess:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable )failure;

/*!
 * @discussion User login with username and password. Registration is mandatory before (see BssAuthmanager:registerWithUsername:password:success:failure).
 * Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h. Password will not be stored.
 * @param username E-mail address of the user which was used for registration.
 * @param password Password of the user which was used for registration.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server (eg user does not exist yet).
 */
- (void)loginWithUsername:(NSString * _Nonnull)username password:(NSString * _Nonnull)password success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via Facebook id. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param access_token User id of the facebook user (access token). Used for the unique identification of the user.
 * @param username Optional info of the user like name, email, etc.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithFacebookToken:(NSString * _Nonnull)access_token username:(NSString * _Nullable)username success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via Google id. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param idToken Provided identity token of the google user.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithGoogleIdToken:(NSString * _Nonnull)idToken success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via Apple. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param idToken Provided identiy token of the Apple user.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithAppleIdToken:(NSString * _Nonnull)idToken success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via VodafoneWallet. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param vodafoneWalletId Customer id of ther vodafone user.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithVodafoneWalletId:(NSString * _Nonnull)vodafoneWalletId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via MVA. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param vodafoneMVAId Customer id of ther vodafone user.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithVodafoneMVAId:(NSString * _Nonnull)vodafoneMVAId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via BBVA. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param bbvaWalletId Unique id of ther BBVA user.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithBBVAWalletId:(NSString * _Nonnull)bbvaWalletId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via Intesa. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param intesaWalletId Unique id of the Intesa user.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithIntesaWalletId:(NSString * _Nonnull)intesaWalletId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via PSA. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param psaId Unique id of the PSA user.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithPSAId:(NSString * _Nonnull)psaId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via S-Loyalty. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param sloyaltyId Unique id of the S-Loyalty user.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithSLoyaltyId:(NSString * _Nonnull)sloyaltyId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Login via HappyDo. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param happydoId Unique id of the user.
 * @param loginType login type that specifies the happy do app.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithHappyDoId:(NSString * _Nonnull)happydoId loginType:(NSString * _Nonnull)loginType success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Generic Login with ID and LoginType. Login status can be checked via the 'loggedIn' property in BssAuthmanager.h or the loginType property in BssDatabase.h.
 * @param uniqueId Unique id of the user.
 * @param loginType login type that specifies the  app.
 * @param success Block which is called after a successful login.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginWithId:(NSString * _Nonnull)uniqueId loginType:(NSString * _Nonnull)loginType success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Delete
/*!
 * @discussion Delete user account including all cards on the server. Do no use this method for a log out (use BssAuthManager:loginAnonymousWithSuccess:failure instead).
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)deleteAccountWithSuccess:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Login Token
/*!
 * @discussion Get a JWT login token for a specific app type. The received token can be used to merge logins using BssAuthManager:mergeLoginWithLoginToken:success:failure.
 * @param appType The app type to be merged.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginTokenForAppType:(NSString * _Nonnull)appType success:(void (^_Nullable)(BssMobilePocketLoginToken * _Nonnull loginToken))success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Merge Login
/*!
 * @discussion Merge login using a received JWT login token from BssAuthManager:loginTokenForAppType:success:failure.
 * @param loginToken The given jwt token for the apptype to be merged.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)mergeLoginWithLoginToken:(BssMobilePocketLoginToken * _Nonnull)loginToken success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

- (void)mergeLoginWithLoginCredentials:(BssLoginCredentials * _Nonnull)loginCredentials success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Register
/*!
 * @discussion User registration with username and password. Password will not be stored.
 * Existing users (after a successful registration) can login using the BssAuthManager:loginWithUsername:password:success:failure method.
 * @param username E-mail address of the user.
 * @param password Password, chosen by the user. Minimum 6 characters required.
 * @param success Block which is called after a successful regsitration.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server (eg user already exists).
 */
- (void)registerWithUsername:(NSString * _Nonnull)username password:(NSString * _Nonnull)password success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Change / Forgot Password
/*!
 * @discussion Service to send an existing user (after a successful registration) a generated password via e-mail. It is recommended to advise the user to change this password after the login.
 * User can change this password after the login by BssAuthmanager:changePassword:oldPassword:newPassword:success:failure.
 * This method can only be used if the user registrated using registerWithUsername:password:success:failure.
 * @param username E-mail address of the user.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)forgotPasswordForUsername:(NSString * _Nonnull)username success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Service to change the password of an existing user.
 * This method can only be used if the user registrated using registerWithUsername:password:success:failure.
 * @param oldPassword Existing password of the user, which should be exchanged with the new one.
 * @param newPassword New password of the user. Minimum 6 characters required.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)changePassword:(NSString * _Nonnull)oldPassword newPassword:(NSString * _Nonnull)newPassword success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Migration
/*!
 * @discussion Migration of an existing user of the hub to the BssBaseKit. Database migration has to be handled manually.
 * @param mobileClientId Existing mobile client id of the user.
 * @param refreshToken Existing refresh token.
 * @param accessToken Existing access token.
 * @param pushToken Optional existing push token.
 * @param loginType Existing login type.
 * @param userName Optional existing user name of the user.
 */
- (void)migrateWithMobileClientId:(NSString * _Nonnull)mobileClientId refreshToken:(NSString * _Nonnull)refreshToken accessToken:(NSString * _Nonnull)accessToken pushToken:(NSString * _Nullable)pushToken loginType:(NSString * _Nonnull)loginType userName:(NSString * _Nullable)userName;
@end
