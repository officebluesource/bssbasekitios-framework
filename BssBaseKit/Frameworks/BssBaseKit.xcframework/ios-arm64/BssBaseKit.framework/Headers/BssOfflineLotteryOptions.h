//
//  BssOfflineLotteryOptions.h
//  BssBaseKit
//
//  Created by Hager Florian on 25.02.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssOfflineLotteryOptions : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * title;
@property (strong, nonatomic, nonnull) NSString * descr;
@property (strong, nonatomic, nonnull) NSArray<NSString *> * values;

@end

NS_ASSUME_NONNULL_END
