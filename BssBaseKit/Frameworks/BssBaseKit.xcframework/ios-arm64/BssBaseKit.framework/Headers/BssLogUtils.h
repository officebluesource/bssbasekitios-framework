//
//  BssLogUtils.h
//  BssBaseKit
//
//  Created by Florian Hager on 12.03.18.
//  Copyright © 2018 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BssLogUtils : NSObject
+ (void)logMessage:(NSString * _Nonnull)message currentFunction:(NSString * _Nonnull)currentFunction;
+ (void)logMessage:(NSString * _Nonnull)message type:(NSString * _Nullable)type currentFunction:(NSString * _Nonnull)currentFunction;

+ (void)reset;

// Set this property to handle logging on the console yourself.
// If not imeplemented, a NSLog will be called automatically.
@property (nonatomic, class, copy) void (^ _Nullable logListener)(NSString * _Nonnull log);

@end
