//
//  BssWebserviceClient.h
//  BssBaseKit
//
//  Created by Florian Hager on 09.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BssBase.h"
#import "BssImageStore.h"

// DTOs
#import "BssCountry.h"
#import "BssSelectedCountry.h"
#import "BssClientConfiguration.h"
#import "BssClientSettings.h"
#import "BssCardType.h"
#import "BssCardTypeRequestInfo.h"
#import "BssCard.h"
#import "BssCreateCardContainer.h"
#import "BssCardBonusProgram.h"
#import "BssBarcodePreview.h"
#import "BssItem.h"
#import "BssRetailer.h"
#import "BssRedemptionInformation.h"
#import "BssRedeemedCouponResponse.h"
#import "BssCardUser.h"
#import "BssCardUserBalances.h"
#import "BssItemDisplayedLog.h"
#import "BssLog.h"
#import "BssRetailerStore.h"
#import "BssItemFilters.h"
#import "BssItemOrder.h"
#import "BssReceiptDetails.h"
#import "BssReceipt.h"
#import "BssReceiptStub.h"
#import "BssReceiptFavorite.h"
#import "BssHoferBankCardAutoSubscribe.h"
#import "BssChaChingTransactionPosition.h"
#import "BssChaChingTransactionAttributes.h"
#import "BssLoyaltyPromotion.h"
#import "BssLotteryParticipantResource.h"
#import "BssCPCardType.h"
#import "BssCurrenciesInfoModel.h"
#import "BssChaChingTransactionSummary.h"
#import "BssCatalogElementType.h"
#import "BssCatalogElement.h"
#import "BssCreateInsurance.h"
#import "BssInsurance.h"
#import "BssUpdateInsurance.h"
#import "BssCatalogElementShareToken.h"

typedef void (^BssWebserviceCatalogElementImageDownloadFailureBlock)(NSArray<NSError *> * _Nullable errors, NSArray<BssCatalogElement *> * _Nullable erroredCatalogElements);

typedef enum : NSUInteger {
    BssCatalogElementTypeRequestableFilterOff,
    BssCatalogElementTypeRequestableFilterRequestable,
    BssCatalogElementTypeRequestableFilterNonRequestable,
} BssCatalogElementTypeRequestableFilter;


@protocol BssCatalogElementImageDownloadDelegate <NSObject>
@required
- (void)didDownloadImageOfCatalogElement:(BssCatalogElement * _Nonnull)catalogElement;
@end

@interface BssWebserviceClient : NSObject

/*!
 * @discussion Delegate property to receive callbacks of the BssCardImageDownloadDelegate protocol.
 */
@property (nonatomic, weak) id _Nullable delegate;

/*!
 * @discussion Use this instance for all method calls inside this class.
 */
+ (instancetype _Nonnull )sharedInstance;

/*!
 * @discussion Sets the requestId to zero.
 */
+ (void)reset;

/*!
 * @discussion Cancel all pending operations..
 */
- (void)cancelAllQueuedOperations;

#pragma mark - Interceptor
// Set this property to handle responses by the host app.
// If not imeplemented, the responses are handled by the base.
@property (nonatomic, class, copy) BOOL (^ _Nonnull responseInterceptor)(NSInteger statuscode, NSError * _Nullable error);


#pragma mark - Countries
/*!
 * @discussion Asynchrounous call to get a list of supported countries of type BssCountryDTO.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)countriesWithSuccess:(void (^_Nullable)(NSArray<BssCountry *> * _Nonnull countries))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Update the selected countries, chosen by the user.
 * @param selectedCountries Array of SelectedCountryDTOs which contains the ISO code of the country.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)updateSelectedCountries:(NSArray<BssSelectedCountry *> * _Nonnull)selectedCountries success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Client Configurations
/*!
 * @discussion Asynchrounous call to get the current configurations for the client. Result will be stored to Database.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)clientConfigurationsWithSuccess:(void (^_Nullable)(NSArray<BssClientConfiguration *> * _Nonnull clientConfigurations))success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Client Settings
/*!
 * @discussion Asynchrounous call to get the current settings for the client. Result will be stored to Database.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)clientSettingsWithSuccess:(void (^_Nullable)(NSArray<BssClientSettings *> * _Nonnull clientSettings))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchrounous call to get a specific setting for the client. Result will be stored to Database.
 * @param key The defined key for the desired setting value.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)clientSettingWithKey:(NSString * _Nonnull)key success:(void (^_Nullable)(BssClientSettings * _Nonnull value))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchrounous call to set a specific setting for the client. Updated Setting will be stored to Database on success.
 * @param value The given value
 * @param key The given key
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)updateClientSettingWithValue:(NSString * _Nonnull)value forKey:(NSString * _Nonnull)key success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Mobile Client
/*!
 * @discussion Asynchrounous call to update the mobile client.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)updateMobileClientWithSuccess:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Images
/*!
 * @discussion Asynchronous call to upload an image. If successful, an BssImage with the meta data will be returned.
 * @param imageData The image data to be uploaded.
 * @param success Block which is called after the webservice request succeeded containing a BssImage.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)uploadImageWithData:(NSData * _Nonnull)imageData success:(void (^_Nullable)(BssImage * _Nonnull image))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to download all missing card images of all locally stored (created) cards. Cards will be automatically updated in core data.
 * This method implements the didDownloadImageForCard: protocol. Use this eg in case of an UI update after a download.
 * @param alphabetically YES/NO if cards are sorted alphabetically or by orderIndex. So the images are loaded. 
 * @param completed Block which is called after download of all missing card images have been completed, containing the error in case of a download of an image failed.
 */
- (void)loadMissingCatalogElementImagesAlphabetically:(BOOL)alphabetically completed:(BssWebserviceCatalogElementImageDownloadFailureBlock _Nullable)completed;

/*!
 * @discussion Asynchronous call to download all missing barcode images of all locally stored (created) cards. Cards will be automatically updated in core data.
 * @param alphabetically YES/NO if cards are sorted alphabetically or by orderIndex. So the images are loaded. 
 * @param completed Block which is called after download of all missing barcode images have been completed, containing the error in case of a download of an image failed.
 */
- (void)loadMissingBarcodeImagesAlphabetically:(BOOL)alphabetically completed:(BssWebserviceCatalogElementImageDownloadFailureBlock _Nullable)completed;


#pragma mark - Catalog Element Types
/*!
 * @discussion Asynchonous call to get catalog element types filtered by a given criteria.
 * @param pageIndex Current page of loaded items. Page index start at 0.
 * @param pageSize Defines the number of loaded previews per page. Result may be a smaller count if not enough previews are available.
 * @param filter Enum to differ between requestable and non-requestable catalog element types.
 * @param searchText Custom string that defines the search criteria. Result are previews that contain this string.
 * @param success Block which is called after the webservice request succeeded containing an array of catalog element types.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)catalogElementTypesForPageIndex:(int)pageIndex pageSize:(int)pageSize requestableFilter:(BssCatalogElementTypeRequestableFilter)filter filters:(BssItemFilters * _Nullable)filters searchText:(NSString *_Nullable)searchText success:(void (^_Nullable)(NSArray<BssCatalogElementType *> * _Nonnull catalogElementTypes))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get the featured catalog element types. Returning result of the hub depends on the following priority order: selectedCountries (set via selectedCountriesWithSuccess.:failure), mcc, ip, device language.
 * @param filters Optional filters, that specify the subset of the catalog element types that should be returned. The following filters are supported: COUNTRY.{countryIsoCode}
 * @param success Block which is called after the webservice request succeeded containing an array of catalog element types.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)featuredCatalogElementTypesWithFilters:(BssItemFilters * _Nullable)filters success:(void (^_Nullable)(NSArray<BssCatalogElementType *> * _Nonnull catalogElementTypes))success failure:(BssFailureBlock _Nullable)failure NS_SWIFT_NAME(featuredCatalogElementTypes(filters:success:failure:));

/*!
 * @discussion Asynchonous call to get the catalog element type for a given id.
 * @param uniqueId The id of the catalog element type.
 * @param success Block which is called after the webservice request succeeded containing the requested catalog element type.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)catalogElementTypeWithId:(NSString * _Nonnull)uniqueId success:(void (^_Nullable)(BssCatalogElementType * _Nonnull catalogElementType))success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Cardtypes
/*!
 * @discussion Asynchonous call to get the card type requeest info for a given id.
 * @param cardTypeId The id of the card type.
 * @param success Block which is called after the webservice request succeeded containing the requested cardtype request info.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)cardTypeRequestInfoWithId:(NSString * _Nonnull)cardTypeId success:(void (^_Nullable)(BssCardTypeRequestInfo * _Nonnull cardTypeRequestInfo))success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Catalog Elements
/*!
 * @discussion Asynchronous call to get an hub-updated set of cards from the current client. Cards that are currently locally stored in core data will be returned immediately. Cards will be automatically updated in core data.
 * @param success Block which is called after the webservice request succeeded containing an array of the requested cards.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 * @return Cards which are currently stored in core data, before the update.
 */
- (NSArray<BssCatalogElement *> * _Nonnull)catalogElementsWithSuccess:(void (^_Nullable)(NSArray<BssCatalogElement *> * _Nonnull updatedCards))success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Share Catalog Elements
/*!
 * @discussion Asynchronous call to create a shareToken for an catalog element.
 * @param publicId The public id of the catalog element to be shared.
 * @param success Block which is called after the webservice request succeeded containing an ShareToken object.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)tokenForSharedCatalogElementWithId:(NSString * _Nonnull)publicId success:(void (^_Nonnull)(BssCatalogElementShareToken * _Nonnull shareToken))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get the shared catalog element with a previously created token.
 * @param token The shared token which was created by the client before.
 * @param success Block which is called after the webservice request succeeded containing the shared catalog element.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)createSharedCatalogElementWithToken:(NSString * _Nonnull)token success:(void (^_Nonnull)(BssCatalogElement * _Nonnull sharedCatalogElement))success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Insurance
/*!
 * @discussion Asynchronous call to create (POST) an insurance. The created insurance will be updated, stored in database and returned in the success block with an unique id.
 * Custom Back image will be uploaded automatically inside this method.
 * @param insurance The insurance which will be created on the hub.
 * @param backImageForUpload The custom back image
 * @param success Block which is called after the webservice request succeeded containing the requested insurance.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)createInsurance:(BssCreateInsurance * _Nonnull)insurance backImageForUpload:(UIImage * _Nullable)backImageForUpload success:(void (^_Nullable)(BssInsurance * _Nonnull createdInsurance))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to delete a specific insurance from the hub. Insurance will also be deleted in database.
 * @param insuranceId Unique id of the insurance.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)deleteInsuranceWithId:(NSString * _Nonnull)insuranceId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to update (PUT) a card. The card will also be updated in database on success. Custom Card images will be uploaded automatically inside this method. Also the retailer's logo will be downloaded after the card is updated on server.
 * @param insurance The insurance object
 * @param insuranceUpdate The insurance properties, which have to be updated
 * @param backImageForUpload The custom backImage
 * @param success Block which is called after the webservice request succeeded containing the updated insurance.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)updateInsurance:(BssInsurance * _Nonnull)insurance insuranceUpdate:(BssUpdateInsurance * _Nonnull)insuranceUpdate backImageForUpload:(UIImage * _Nullable)backImageForUpload success:(void (^_Nullable)(BssInsurance * _Nonnull createdInsurance))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get a specific insurance from the hub. If insurance exists, it will be updated in database and returned in the success block.
 * @param insuranceId Unique id of the desired insurance.
 * @param success Block which is called after the webservice request succeeded containing the requested insurance.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)insuranceWithId:(NSString * _Nonnull)insuranceId success:(void (^_Nullable)(BssInsurance * _Nonnull updatedInsurance))success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Cards
/*!
 * @discussion Asynchronous call to create (POST) a card. The created card will be updated, stored in database and returned in the success block with an unique id and optional iteminfolist set by the hub.
 * Custom Card images will be uploaded automatically inside this method.
 * @param card The card which will be created on the hub. This card will be updated automatically (uniqueId, itemInfoList)
 * @param success Block which is called after the webservice request succeeded containing the requested card icluding the unique id and the iteminfolist.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)createCard:(BssCard * _Nonnull)card success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to create a requestable card (SUNC). The card inside the cardContainer will be updated, stored in database and returned in the success block with an unique id and an optional iteminfolist set by the hub.
 * @param cardContainer container object that contains the card object and request information object. The card inside the cardContainer will be updated automatically (uniqueId, itemInfoList)
 * @param success Block which is called after the webservice request succeeded containing the requested card.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)createRequestableCard:(BssCreateCardContainer * _Nonnull)cardContainer success:(void (^_Nullable)(BssCard * _Nonnull createdCard))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to update (PUT) a card. The card will also be updated in database on success. Custom Card images will be uploaded automatically inside this method. Also the retailer's logo will be downloaded after the card is updated on server.
 * @param card The card shall will be updated on the hub.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)updateCard:(BssCard * _Nonnull)card success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get a specific card from the hub. If card exists, it will be updated in database and returned in the success block.
 * @param cardId Unique id of the desired card.
 * @param success Block which is called after the webservice request succeeded containing the requested card.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 * @return Card with the given id which is currently stored in core data.
 */
- (BssCard * _Nullable)cardWithId:(NSString * _Nonnull)cardId success:(void (^_Nullable)(BssCard * _Nonnull updatedCard))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to delete a specific card from the hub. Card will also be deleted in database.
 * @param cardId Unique id of the card.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)deleteCardWithId:(NSString * _Nonnull)cardId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get the barcode image of a specific card. This requires a barcode number and a barcode type.
 * @param cardId Unique id of the card.
 * @param success Block which is called after the webservice request succeeded containing the result image, cachetype and image url.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)barcodeImageForCardWithId:(NSString * _Nonnull)cardId maxWidth:(int)maxWidth maxHeight:(int)maxHeight success:(void (^ _Nullable)(UIImage * _Nonnull image, BssImageStorageType storageType, NSURL * _Nonnull imageUrl))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get the bonus program of a specific card. This requires a barcode number and a barcode type.
 * @param cardId Unique id of the card.
 * @param success Block which is called after the webservice request succeeded containing the bonus program.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)bonusProgramForCardWithId:(NSString * _Nonnull)cardId success:(void (^_Nullable)(BssCardBonusProgram * _Nonnull bonusProgram))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to login for a security extension like REWE or Nectar, etc. Returns a card in the success block with the needed properties set by the server.
 * @param username username for the security extension.
 * @param password password for the security extension.
 * @param cardId Unique id of the card.
 * @param success Block which is called after the webservice request succeeded containing the updated card.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)loginForSecurityExtensionWithUsername:(NSString* _Nonnull)username andPassword:(NSString* _Nonnull)password cardId:(NSString* _Nonnull)cardId success:(void (^ _Nullable)(BssCard * _Nullable))success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Barcode Previews
/*!
 * @discussion Asynchronous call to get barcode preview images. Received barcodes types depend on the given number. Barcode images will be scaled down by server as good as possible.
 * @param number The given barcode number. Depending on this number, the server returns a set of suitable barcode types.
 * @param maxWidth Maximum width of the barcode.
 * @param maxHeight Maximum height of the barcode.
 * @param success Block which is called after the webservice request succeeded containing the requested barcodes previews.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)barcodePreviewsForNumber:(NSString * _Nonnull)number maxWidth:(int)maxWidth maxHeight:(int)maxHeight success:(void (^_Nullable)(NSArray<BssBarcodePreview *> * _Nonnull barcodePreviews))success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Items
/*!
 * @discussion Asynchronous call to receive paged items filtered by a given criteria and ordered either bei default, user specific or by distance from a given location
 * @param pageIndex Current page of loaded items. Page index start at 0.
 * @param pageSize Defines the number of loaded items per page. Result may be a smaller count if not enough items are available.
 * @param filters Customized search criteria for filtering the loaded items. Use BssItemFiltersDTO to create a suitable filter.
 * @param order Determines the sort order of the loaded items. Use BssItemOrderDTO to define a sort order, which can be 'default', 'user specific' or by 'distance' with a given location.
 * @param success Block which is called after the webservice request succeeded containing an array of the requested items.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)filteredItemsForPageIndex:(int)pageIndex pageSize:(int)pageSize filters:(BssItemFilters * _Nullable)filters order:(NSString * _Nullable)order success:(void (^_Nullable)(NSArray<BssItem *> * _Nonnull items))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get a selected item for a given itemId.
 * @param itemId Unique id of the desired item.
 * @param success Block which is called after the webservice request succeeded containing the requested item.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)itemWithId:(NSString * _Nonnull)itemId success:(void (^_Nullable)(BssItem * _Nonnull item))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get the retailer behind an item.
 * @param itemId Unique id of the item.
 * @param success Block which is called after the webservice request succeeded containing the requested retailer.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)retailerForItemWithId:(NSString * _Nonnull)itemId success:(void (^_Nullable)(BssRetailer * _Nonnull retailer))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get a redemptionInfo of an item.
 * @param itemId Unique id of the item.
 * @param success Block which is called after the webservice request succeeded containing the requested coupon.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)redemptionInformationForItemWithId:(NSString * _Nonnull)itemId success:(void (^_Nullable)(BssRedemptionInformation * _Nonnull coupon))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to redeem a coupon of an item. Width and height define the size of the barcode image that may return. Barcode image will be scaled down by server as good as possible.
 * @param itemId Unique id of the item.
 * @param maxWidth Maximum width of the barcode image that may return.
 * @param maxHeight Maximum height of the barcode image that may return.
 * @param success Block which is called after the webservice request succeeded containing the requested coupon result.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)redeemCouponForItemWithId:(NSString * _Nonnull)itemId maxWidth:(NSNumber * _Nullable)maxWidth maxHeight:(NSNumber * _Nullable)maxHeight success:(void (^_Nullable)(BssRedeemedCouponResponse * _Nullable redeemedCouponResponse))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to redeem a goodie of an item.
 * @param itemId Unique id of the item.
 * @param success Block which is called after the webservice request succeeded containing the requested transaction summary result.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)redeemGoodieForItemWithId:(NSString * _Nonnull)itemId success:(void (^_Nullable)(BssChaChingTransactionSummary * _Nonnull transactionSummary))success failure:(BssFailureBlock _Nullable)failure;

- (void)participateForOfflineLotteryWithParticipantResource:(BssLotteryParticipantResource * _Nonnull)participantResource itemId:(NSString * _Nonnull)itemId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Card User
/*!
 * @discussion Asynchronous call to the card user of the current client.
 * @param success Block which is called after the webservice request succeeded containing the requested card user.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)cardUserWithSuccess:(void (^_Nullable)(BssCardUser * _Nonnull cardUser))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to the card user of the current client.
 * @param success Block which is called after the webservice request succeeded containing the requested card user balances.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)cardUserBalancesWithSuccess:(void (^_Nullable)(BssCardUserBalances * _Nonnull cardUserBalances))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call for card user contribution (cha-ching).
 * @param success Block which is called after the webservice request succeeded containing the requested card user balances.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)cardUserContributionChaChingWithChaChingTransactionAttributes:(BssChaChingTransactionAttributes * _Nonnull)chaChingTransactionAttributes success:(void (^_Nullable)(BssCardUserBalances * _Nonnull cardUserBalances))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call for card user currencies.
 * @param success Block which is called after the webservice request succeeded containing the requested card user currencies.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)cardUserCurrencies:(void (^_Nullable)(BssCurrenciesInfoModel * _Nonnull))success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Logs
/*!
 * @discussion Asynchonous call to send an hub related log (with a debounce of 2 seconds). If more logs will be sent in a very short time (like when scrolling down items), the logs are collected and sent after a debounce of 2 seconds
 * @param action String representing the action behind the log, eg. item_clicked.
 * @param loggedObjectId String represetning the id of an object, eg. itemId.
 */
- (void)sendLogWithAction:(NSString * _Nonnull)action loggedObjectId:(NSString * _Nullable)loggedObjectId;

/*!
 * @discussion Asynchonous call to send an hub related log (with a debounce of 2 seconds). If more logs will be sent in a very short time (like when scrolling down items), the logs are collected and sent after a debounce of 2 seconds
 * @param action String representing the action behind the log, eg. item_clicked.
 * @param loggedObjectId String represetning the id of an object, eg. itemId.
 * @param itemDisplayedListIndex  represetning  item's rank in the list of items provided to the client.
 * @param itemDisplayedScreenPosition String represetning item's display position. Allowed values are 'LEFT' or 'RIGHT'.
 */
- (void)sendLogWithAction:(NSString * _Nonnull)action loggedObjectId:(NSString * _Nullable)loggedObjectId itemDisplayedListIndex:(int)itemDisplayedListIndex itemDisplayedScreenPosition:(NSString * _Nullable)itemDisplayedScreenPosition;


#pragma mark - Retailer
/*!
 * @discussion Asynchronous call to get the retailer of a given id.
 * @param retailerId The uniqie id of the desired retailer.
 * @param success Block which is called after the webservice request succeeded containing the requested retailer.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)retailerWithId:(NSString * _Nonnull)retailerId success:(void (^_Nullable)(BssRetailer * _Nonnull retailer))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get the stores behind a retailer.
 * @param retailerId The unique id of the retailer.
 * @param success Block which is called after the webservice request succeeded containing an array of the requested retailer stores.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)storesForRetailerWithId:(NSString * _Nonnull)retailerId searchText:(NSString * _Nullable)searchText latitude:(NSString * _Nullable)latitude longitude:(NSString * _Nullable)longitude success:(void (^_Nullable)(NSArray<BssRetailerStore *> * _Nonnull stores))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get the stores behind a retailer.
 * @param retailerId The unique id of the retailer.
 * @param success Block which is called after the webservice request succeeded containing an array of the requested retailer stores.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 * @param pageIndex The index of the page to load
 * @param pageSize The size of the page to load
 */
- (void)storesForRetailerWithId:(NSString * _Nonnull)retailerId searchText:(NSString * _Nullable)searchText latitude:(NSString * _Nullable)latitude longitude:(NSString * _Nullable)longitude pageIndex:(NSString * _Nullable)pageIndex pageSize:(NSString * _Nullable)pageSize success:(void (^_Nullable)(NSArray<BssRetailerStore *> * _Nonnull stores))success failure:(BssFailureBlock _Nullable)failure;


#pragma mark - Terms
/*!
 * @discussion Asynchronous call to revoke the terms and conditions of the hub.
 * @param success Block which is called after the webservice request succeeded containing the requested terms and conditions.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)revokeTermsAndConditionsWithSuccess:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Receipt
/*!
 * @discussion Asynchronous call to get all receipts from server with a specific search request
 * @param cardId The id of a HOFER card
 * @param searchString A string with the search text
 * @param success Block which is called after the webservice request succeeded. It contains an array of receipts which match the search request.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)searchReceiptsWithCardId:(NSString* _Nonnull)cardId searchString:(NSString* _Nonnull)searchString success:(void (^_Nullable)(NSArray<BssReceipt*>* _Nonnull receipts))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get all receipts from server for a specific card
 * @param cardId The id of a HOFER card
 * @param limit The amount of cards
 * @param startId The id of an receipt.
 * @param success Block which is called after the webservice request succeeded. It contains an array of receipts, starting with the receipts, which has the startId, and the size of the array is equal to the limit parameter. If the startId is not defined the first receipt will be the newest one.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)getReceiptListWithCardId:(NSString* _Nonnull)cardId limit:(NSString* _Nonnull)limit startId:(NSString* _Nullable)startId success:(void (^_Nullable)(NSArray<BssReceipt*>* _Nonnull receipts))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to create a receipt
 * @param cardId The id of a HOFER card
 * @param stub A class which contains the receipt number and the creation date
 * @param success Block which is called after the webservice request succeeded. It contains an array of  receipts.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)createReceiptWithCardId:(NSString* _Nonnull)cardId receiptStub:(BssReceiptStub* _Nonnull) stub success:(void (^_Nullable)(NSArray<BssReceipt*>* _Nonnull receipts))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get receipt details from Server
 * @param cardId The id of a HOFER card
 * @param receiptId The id of an receipt
 * @param success Block which is called after the webservice request succeeded: It contains an receipt detail object which refers to the requested receiptId.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)getReceiptDetailWithCardId:(NSString* _Nonnull)cardId receiptId:(NSString* _Nonnull)receiptId success:(void (^_Nullable)(BssReceiptDetails* _Nonnull receiptDetails))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronos call to delete an receipt from server
 * @param cardId The id of a HOFER card
 * @param receiptId The id of an receipt
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)deleteReceiptWithCardId:(NSString* _Nonnull)cardId receiptId:(NSString* _Nonnull)receiptId success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to get all favourites for a specific card from server
 * @param cardId The id of a HOFER card
 * @param success Block which is called after the webservice request succeeded. It contains an array of receipt details which are favorite.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)getFavouriteReceiptListWithCardId:(NSString* _Nonnull)cardId success:(void (^_Nullable)(NSArray<BssReceiptDetails*>* _Nonnull receiptDetails))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion update a receipt's favorite status
 * @param cardId The id of a HOFER card
 * @param receiptId The id of an receipt
 * @param fav An BssReceiptFavorite object, which contains a single boolean field, favorite or not
 * @param success Block which is called after the webservice request succeeded. It contains an array of receipt details.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)updateFavouriteStatusWithCardId:(NSString* _Nonnull)cardId receiptId:(NSString* _Nonnull)receiptId receiptFav:(BssReceiptFavorite* _Nonnull) fav success:(void (^_Nullable)(NSArray<BssReceiptDetails*>* _Nonnull receiptDetails))success failure:(BssFailureBlock _Nullable)failure;

/*!
* @discussion get search suggestions from server for a specific search request
* @param cardId The id of a HOFER card
* @param searchString A string with the search text
* @param success Block which is called after the webservice request succeeded. It contains an array with suggestion strings.
* @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
*/
- (void)getSearchSuggestionsWithCardId:(NSString* _Nonnull)cardId searchString:(NSString* _Nonnull)searchString success:(void (^_Nullable)(NSArray<NSString*>* _Nonnull searchSuggestions))success failure:(BssFailureBlock _Nullable)failure;

- (void)updateHoferCardBankCardsAutoSubscribeWithCardId:(NSString* _Nonnull)cardId autoSubscribe:(BOOL)autoSubscribe success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

- (void)getHoferCardBankCardsAutoSubscribeWithCardId:(NSString* _Nonnull)cardId success:(void (^_Nullable)(BssHoferBankCardAutoSubscribe* _Nonnull autoSubscribe))success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Cha-Ching
/*!
 * @discussion Asynchronous call to receive paged transactions filtered by a given criteria.
 * @param cardId Unique id of the card.
 * @param pageIndex Current page of transactions to be loaded. Page index start at 0.
 * @param pageSize Defines the number of loaded transactions per page. Result may be a smaller count if not enough transactions are available.
 * @param success Block which is called after the webservice request succeeded containing an array of the requested transactions.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
*/
- (void)transactionPositionsForChaChingWithCardId:(NSString * _Nonnull)cardId pageIndex:(int)pageIndex pageSize:(int)pageSize success:(void (^_Nullable)(NSArray<BssChaChingTransactionPosition *> * _Nonnull items))success failure:(BssFailureBlock _Nullable)failure;

/*!
 * @discussion Asynchronous call to validated qr code from a receipt and and gets points for the purchase.
 * @param cardId Unique id of the card.
 * @param qrCode QR code to validate.
 * @param success Block which is called after the webservice request succeeded.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
 */
- (void)pointsFromReceiptWitCardId:(NSString * _Nonnull)cardId qrCode:(NSString* _Nonnull)qrCode success:(void (^_Nullable)(BssChaChingTransactionSummary* _Nonnull scan2CollectResult))success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Collect Points
/*!
 * @discussion Asynchronous call to receive paged cardtypes for collect points.
 * @param pageIndex Current page of transactions to be loaded. Page index start at 0.
 * @param pageSize Defines the number of loaded transactions per page. Result may be a smaller count if not enough cardtypes are available.
 * @param success Block which is called after the webservice request succeeded containing an array of cpcardtypes.
 * @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
*/
- (void)collectPointsCardTypesForPageIndex:(int)pageIndex pageSize:(int)pageSize success:(void (^_Nullable)(NSArray<BssCPCardType *> * _Nonnull cardTypePreviews))success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Loyalty Promotions
- (void)loyaltyPromotionsWithLatitude:(NSString * _Nonnull)latitude longitude:(NSString * _Nonnull)longitude radius:(int)radius success:(void (^_Nullable)(NSArray<BssLoyaltyPromotion *> * _Nonnull promotions))success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Geolocations
- (void)updateGeolocationWithGeolocation:(BssGeoLocation * _Nullable)geolocation success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

@end
