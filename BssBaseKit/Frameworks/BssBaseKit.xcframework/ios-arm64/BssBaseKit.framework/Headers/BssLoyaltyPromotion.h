//
//  BssLoyaltyPromotion.h
//  BssBaseKit
//
//  Created by Hager Florian on 07.12.20.
//  Copyright © 2020 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssLoyaltyCouponCode.h"

@interface BssLoyaltyPromotion : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * uniqueId;
@property (strong, nonatomic, nullable) NSString <Optional> * partnerId;
@property (strong, nonatomic, nullable) NSString <Optional> * partnerName;
@property (strong, nonatomic, nullable) NSString <Optional> * partnerUrl;
@property (strong, nonatomic, nullable) NSString <Optional> * title;
@property (strong, nonatomic, nullable) NSString <Optional> * caption;
@property (strong, nonatomic, nullable) NSString <Optional> * descr;
@property (strong, nonatomic, nullable) NSString <Optional> * imageUrl;
@property (strong, nonatomic, nullable) NSString <Optional> * logoUrl;
@property (strong, nonatomic, nullable) NSString <Optional> * expires;
@property (strong, nonatomic, nullable) NSString <Optional> * conditionTextShort;
@property (strong, nonatomic, nullable) NSString <Optional> * conditionText;
@property (strong, nonatomic, nullable) BssLoyaltyCouponCode <Optional> * code;

@end
