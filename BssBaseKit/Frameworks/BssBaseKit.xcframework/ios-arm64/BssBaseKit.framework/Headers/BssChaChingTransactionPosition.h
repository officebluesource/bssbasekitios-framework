//
//  BssChaChingTransactionPosition.h
//  BssBaseKit
//
//  Created by Hager Florian on 10.07.20.
//  Copyright © 2020 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssMonetaryAmount.h"
#import "BssChaChingTransaction.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    BssChaChingTransactionPositionTypeUnknown,
    BssChaChingTransactionPositionTypeCredit,
    BssChaChingTransactionPositionTypeDebit
} BssChaChingTransactionPositionType;

@interface BssChaChingTransactionPosition : BssJSONModel

@property (strong, nonatomic, nonnull) BssMonetaryAmount * amount;
@property (strong, nonatomic, nullable) BssMonetaryAmount<Optional> * postedAmount;
@property (strong, nonatomic, nonnull) NSString * type;
@property (strong, nonatomic, nonnull) BssChaChingTransaction* transaction;

- (BssChaChingTransactionPositionType)typeEnum; //ignored

@end

NS_ASSUME_NONNULL_END
