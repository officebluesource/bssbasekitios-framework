//
//  BssBase.h
//  BssBaseKit
//
//  Created by Florian Hager on 12.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^BssFailureBlock)(NSError * _Nullable error);
typedef void (^BssEmptyBlock)(void);

typedef enum : NSUInteger {
    BssEnvironmentProd,
    BssEnvironmentPreprod,
    BssEnvironmentStaging,
    BssEnvironmentDev
} BssEnvironment;

@interface BssBase : NSObject

/**
 * Check if mobile client has already been created.
 */
@property (nonatomic, class, readonly) BOOL mobileClientCreated;

/**
 * Check if user is currently logged in. This also includes the anonymous login.
 */
@property (nonatomic, class, readonly) BOOL loggedIn;

/**
 * Check if user is explicitly logged in anonymous.
 */
@property (nonatomic, class, readonly) BOOL loggedInAnonymous;

/**
 * Returns the initial install version of the app (main bundle). Nil if not set.
 * If not set already, the initialInstallVersion will be set inside the initWithBaseUrl:appVerion:appType:salt:success:failure.
 * An already existing initial install version can be migrated via the migrateInitialInstallVersion: method.
 * The intialInstallVersion will not be reset on an app reset.
 */
@property (nullable, nonatomic, class, readonly) NSString* initialInstallVersion;

/*!
 * @discussion Checks if the T&Cs are outdated. Returns false if the terms have never been accepted. Useful if you want to display an update text of the terms to be accepted.
 * @return True if terms have already been accepted once but status is "not accepted", otherwise false.
*/
@property (nonatomic, class, readonly) BOOL termsOutdated;

/**
 * Returns the forced country code if available, otherwise the country code of the device region.
 * The country code is in the format ISO 3166-1 alpha-2 e.g "AT".
 * Set this property to nil again to use the default value.
 **/
@property (nullable, strong, nonatomic, class, setter=forceIsoCountryCode:) NSString * isoCountryCode;

/**
 * Returns the advirtising id which was set by the client, nil if not set. Make sure to set this property before calling BssBase:initWithBaseUrl or make sure to update the client afterwards.
 * This value will only by available at runtime, so make sure to set this property at least on every app start.
 * It is the client's responsibility to send only valid advertising ids to the hub.
 **/
@property (nullable, strong, nonatomic, class) NSString * advertisingId;

// Values set in initWithBaseUrl:appVersion:appType:salt:useSSLPinning:success:failure
// only available at runtime (will not be stored in database)
@property (nullable, nonatomic, strong, class, readonly) NSURL* baseUrl;
@property (nullable, nonatomic, strong, class, readonly) NSString* appType;
@property (nullable, nonatomic, strong, class, readonly) NSString* appVersion;
@property (nullable, nonatomic, strong, class, readonly) NSString* salt;
@property (nonatomic, class, readonly) BOOL useSSLPinning;

#pragma mark - Terms
/*!
 * @discussion Check before initializing the BssBaseKit, if the Terms & Conditions are already accepted for the given versions.
 * @param termsVersion  The current version of the terms.
 * @param privacyVersion The current version of the confitions.
 * @return True if the terms have already been accepted with the given version, otherwise false.
*/
+ (BOOL)termsAcceptedWithTermsVersion:(NSString * _Nonnull)termsVersion privacyVersion:(NSString * _Nonnull)privacyVersion;

/*!
 * @discussion To be called if the terms and condiions are being accepted by the user.
 * @param termsVersion  The current version of the terms.
 * @param privacyVersion  The current version of the confitions.
*/
+ (void)acceptTermsWithTermsVersion:(NSString * _Nonnull)termsVersion privacyVersion:(NSString * _Nonnull)privacyVersion;

/*!
* @discussion Asynchronous call to revoke the terms and conditions of the hub.
* @param success Block which is called after the webservice request succeeded containing the requested terms and conditions.
* @param failure Block which is called when an error occurs. This could be for example because of a missing internet connection, time out, or an error returned by the server.
*/
+ (void)revokeTermsWithSuccess:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Initialization
/**
 * Setup method to initialize the BssBaseKit. This method only stores runtime values..
 * After the setup is done, a login of the user must be done if not already happened. Login status can be checked via the 'loggedIn' property in BssBase class or the loginType property in BssDatabase class.
 * @param environment Host url that points on a certain server of the hub.
 * @param appVersion Version of the app that uses the BssBaseKit. For every change of the major or minor version, a new salt is required.
 * @param appType App type. Find possible values in BssDefines.h
 * @param salt Salt that suits to the given app version.
 * @param useSSLPinning Decide whether SSL fingerprint check should be done for https urls or not. Will be ignored for non https urls.
 */
+ (void)initWithEnvironment:(BssEnvironment)environment appVersion:(NSString * _Nonnull)appVersion appType:(NSString * _Nonnull)appType salt:(NSString * _Nonnull)salt useSSLPinning:(BOOL)useSSLPinning;

/**
 * When initializing the BssBaseKit, make sure to have  the Terms and Conditions acceped via the BssBase:acceptTermsWithTermsVersion method!
 * Setup method to initialize the BssBaseKit and creating / updating the mobile client. Call this method at the app start before any webservice request on the BssBaseKit is done.
 * After the setup is done, a login of the user must be done if not already happened. Login status can be checked via the 'loggedIn' property in BssBase class or the loginType property in BssDatabase class.
 * @param environment Host url that points on a certain server of the hub.
 * @param appVersion Version of the app that uses the BssBaseKit. For every change of the major or minor version, a new salt is required.
 * @param appType App type. Find possible values in BssDefines.h
 * @param salt Salt that suits to the given app version.
 * @param useSSLPinning Decide whether SSL fingerprint check should be done for https urls or not. Will be ignored for non https urls.
 */
+ (void)initWithEnvironment:(BssEnvironment)environment appVersion:(NSString * _Nonnull)appVersion appType:(NSString * _Nonnull)appType salt:(NSString * _Nonnull)salt useSSLPinning:(BOOL)useSSLPinning success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

/**
 * Setup method to initialize the BssBaseKit. This method only stores runtime values..
 * After the setup is done, a login of the user must be done if not already happened. Login status can be checked via the 'loggedIn' property in BssBase class or the loginType property in BssDatabase class.
 * @param baseUrl Hub server url.
 * @param appVersion Version of the app that uses the BssBaseKit. For every change of the major or minor version, a new salt is required.
 * @param appType App type. Find possible values in BssDefines.h
 * @param salt Salt that suits to the given app version.
 * @param useSSLPinning Decide whether SSL fingerprint check should be done for https urls or not. Will be ignored for non https urls.
 */
+ (void)initWithBaseUrl:(NSURL * _Nonnull)baseUrl appVersion:(NSString * _Nonnull)appVersion appType:(NSString * _Nonnull)appType salt:(NSString * _Nonnull)salt useSSLPinning:(BOOL)useSSLPinning;

/**
 * When initializing the BssBaseKit, make sure to have  the Terms and Conditions acceped via the BssBase:acceptTermsWithTermsVersion method!
 * Setup method to initialize the BssBaseKit and creating / updating the mobile client. Call this method at the app start before any webservice request on the BssBaseKit is done.
 * After the setup is done, a login of the user must be done if not already happened. Login status can be checked via the 'loggedIn' property in BssBase class or the loginType property in BssDatabase class.
 * @param baseUrl Host url that points on a certain server of the hub.
 * @param appVersion Version of the app that uses the BssBaseKit. For every change of the major or minor version, a new salt is required.
 * @param appType App type. Find possible values in BssDefines.h
 * @param salt Salt that suits to the given app version.
 * @param useSSLPinning Decide whether SSL fingerprint check should be done for https urls or not. Will be ignored for non https urls.
 */
+ (void)initWithBaseUrl:(NSURL * _Nonnull)baseUrl appVersion:(NSString * _Nonnull)appVersion appType:(NSString * _Nonnull)appType salt:(NSString * _Nonnull)salt useSSLPinning:(BOOL)useSSLPinning success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

#pragma mark - Reset
/**
 * Reset whole Database, image cache, shared instances and static variables in BssBaseKit.
 */
+ (void)reset;

/**
* Reset image cache, shared instances and static variables in BssBaseKit.
*/
+ (void)resetWithoutDatabaseReset;

/**
 * Remove all entries in keychain bundled to the given mobile client id
 * @param mobileClientId Mobile Client id.
*/
+ (void)cleanUpKeychainForMobileClientWithId:(NSString * _Nonnull)mobileClientId;


#pragma mark - Switch MobileClientId
/**
 * Set client related properties for example to switch clients if multiple clients are being used. The given params will only be overwritten in Database, a required initWithBaseUrl:appVerion:appType:salt:success:failure has to be called manually to create or update the client afterwards. If a new client was created, which can be checked via the loggedIn property, a user login has to be called as well.
 * Be responsible to securely store the current values behind these parameter properties before switching the user by retrieving them from the BssDatabase class.
 * @param mobileClientId Client id of the client to be switched. Nil for a new client.
 */
+ (void)setMobileClientId:(NSString * _Nullable)mobileClientId;

/**
 * Set client related properties for example to switch clients if multiple clients are being used. The given params will only be overwritten in Database, a required initWithBaseUrl:appVerion:appType:salt:success:failure has to be called manually to create or update the client afterwards. If a new client was created, which can be checked via the loggedIn property, a user login has to be called as well.
 * Be responsible to securely store the current values behind these parameter properties before switching the user by retrieving them from the BssDatabase class.
 * @param mobileClientId Client id of the client to be switched. Nil for a new client.
 * @param pushToken Optional existing push token.
 * @param loginType Login type of the client to be switched.
 */
+ (void)setMobileClientId:(NSString * _Nullable)mobileClientId pushToken:(NSString * _Nullable)pushToken loginType:(NSString * _Nullable)loginType;


#pragma mark - Migrations
/**
 * Migrate an already existing initial install version from the app. If initial install version is already set in BssBaseKit, it will be overwritten.
 * @param initialInstallVersion The app's initial install version. Must not be null.
 */
+ (void)migrateInitialInstallVersion:(NSString * _Nonnull)initialInstallVersion;

/**
 * Migrate already accepted terms and conditions with the original timestamp.
 * @param termsVersion Accepted terms version.
 * @param privacyVersion Accepted privacy version.
 * @param timestamp Timestamp then these terms and privacy versions have been accepted.
 */
+ (void)migrateTermsVersion:(NSString * _Nonnull)termsVersion privacyVersion:(NSString * _Nonnull)privacyVersion timestamp:(NSDate * _Nonnull)timestamp;

@end
