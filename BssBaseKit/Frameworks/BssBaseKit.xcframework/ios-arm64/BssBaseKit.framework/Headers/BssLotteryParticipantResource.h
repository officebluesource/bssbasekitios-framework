//
//  BssLotteryParticipantResource.h
//  BssBaseKit
//
//  Created by Hager Florian on 22.02.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssLinkResource.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BssLinkResource;

@interface BssLotteryParticipantResource : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * email;
@property (strong, nonatomic, nullable) NSString <Optional> * phoneNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * selectedOption;
@property (strong, nonatomic, nullable) NSArray<BssLinkResource *> <BssLinkResource, Optional> * acceptedLinks;

@end

NS_ASSUME_NONNULL_END
