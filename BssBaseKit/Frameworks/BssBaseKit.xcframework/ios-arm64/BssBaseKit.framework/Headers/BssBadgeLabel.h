//
//  BssBadgeLabel.h
//  BssBaseKit
//
//  Created by Florian Hager on 18.09.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BssTTTAttributedLabel.h"

@interface BssBadgeLabel : BssTTTAttributedLabel

@property (nonatomic) IBInspectable BOOL usePrimaryColor;

@end
