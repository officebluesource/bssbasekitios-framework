//
//  UIViewController+BssAdditions.h
//  app
//
//  Created by Florian Hager on 28.12.16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BssAdditions)

+ (UIViewController *)BSS_topViewController;
- (BOOL)BSS_isPresentedAsModal;

@end
