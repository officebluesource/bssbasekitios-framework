//
//  NSError+BssWebservice.h
//  BssBaseKit
//
//  Created by Florian Hager on 18.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ERROR_ENUM(NSURLErrorDomain)
{
    BssErrorCodeUserDeleted = 512,
    BssErrorCodeAccountInvalidated = 522,
    BssErrorCodeTermsNotAccepted = 1000,
};

OBJC_EXPORT NSErrorUserInfoKey const BssHttpStatusCodeKey;

@interface NSError (BssWebservice)

+ (NSError *)BSS_errorWithDescription:(NSString *)text code:(NSInteger)code;
+ (NSError *)BSS_errorWithDescription:(NSString *)text code:(NSInteger)code httpStatusCode:(NSInteger)httpStatusCode;
+ (NSError *)BSS_errorNoInternetConnection;
+ (NSError *)BSS_errorTimeOut;
+ (NSError *)BSS_errorTermsNotAccepted;

+ (NSInteger)BSS_errorCodeOfFailedOperationResponseString:(NSString *)responseString;

@end

