//
//  BssUtils.h
//  BssBaseKit
//
//  Created by Florian Hager on 12.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssBase.h"

@interface BssUtils : NSObject

#pragma mark - Hardware
+ (BOOL)biometricAuthenticationEnabled;
+ (BOOL)faceIdAvailable;
+ (BOOL)forceTouchEnabled;

#pragma mark - Common
+ (NSDate * _Nonnull)parseDate:(NSString * _Nonnull)strDate format:(NSString * _Nonnull)format;
+ (NSInteger)daysUntilDate:(NSDate * _Nonnull)date;
+ (void)dialNumber:(NSString * _Nonnull)number;
+ (void)openExternalBrowserWithUrl:(NSString * _Nonnull)url;
+ (void)mailTo:(NSArray<NSString *> * _Nullable)recipients subject:(NSString * _Nullable)subject;
+ (BOOL)hasInternetConnection;

#pragma mark - Controller
+ (BOOL)currentTopControllerIsTabbarRootController;
+ (CGFloat)navbarHeightForViewController:(UIViewController * _Nonnull)controller;

#pragma mark - Crypto
+ (NSData * _Nullable)encryptedDataOfString:(NSString * _Nonnull)string hashKey:(NSString * _Nonnull)hashKey;
+ (NSString * _Nullable)stringOfEncryptedData:(NSData * _Nonnull)encryptedData hashKey:(NSString * _Nonnull)hashKey;

#pragma mark - File Manager
+ (BOOL)deleteFileAtPathOnDisk:(nullable NSString *)path;

#pragma mark - Base Urls
+ (BOOL)isCurrentBaseUrlProdUrl;
+ (BOOL)isCurrentBaseUrlTestUrl;
+ (BOOL)isCurrentBaseUrlCustomUrl;
+ (BOOL)isUrlProdUrl:(NSString * _Nonnull)urlString;
+ (BOOL)isUrlTestUrl:(NSString * _Nonnull)urlString;
+ (BOOL)isUrlCustomUrl:(NSString * _Nonnull)urlString;
+ (NSArray <NSString *> * _Nonnull)allBaseUrls;
+ (NSArray <NSString *> * _Nonnull)prodBaseUrls;
+ (NSArray <NSString *> * _Nonnull)testBaseUrls;
+ (NSString * _Nonnull)customBaseUrl;
+ (NSString * _Nullable)easyDescriptionOfBaseUrl:(NSString * _Nonnull)urlString;

@end
