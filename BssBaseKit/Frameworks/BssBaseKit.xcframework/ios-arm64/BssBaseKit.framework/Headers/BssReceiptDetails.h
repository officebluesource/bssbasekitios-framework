//
//  BssPSAReceipt.h
//  BssPSALoyalty
//
//  Created by Viacheslav Korkhonen on 08.03.19.
//  Copyright © 2019 bluesource - mobile solutions gmbh. All rights reserved.
//
#import "BssReceipt.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssReceiptDetails : BssReceipt

@property (strong, nonatomic, nullable) NSArray<BssReceiptFooterContent *> <BssReceiptFooterContent, Optional> * footerContent;
@property (strong, nonatomic, nullable) NSString <Optional> * footerEnding;
@property (strong, nonatomic, nullable) NSString <Optional> * header;
@property (strong, nonatomic, nullable) NSString <Optional> * meansOfPayment;
@property (strong, nonatomic, nonnull) NSString <Ignore> * cardId;
@property (strong, nonatomic, nullable) NSArray<BssReceiptProduct *> <BssReceiptProduct, Optional> * products;

#pragma mark - Additions & Helper
- (NSInteger)guaranteeItemsCount;

@end

NS_ASSUME_NONNULL_END
