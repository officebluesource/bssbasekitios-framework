//
//  BssCatalogElementImageDownloadInfo.h
//  BssBaseKit
//
//  Created by Hager Florian on 14.07.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BssCatalogElementImageDownloadInfo : NSObject
@property (nonatomic) BOOL authRequired;
@property (nonatomic) BOOL imageNeeded;
@property (strong, nonatomic, nonnull) NSString * imageUrl;
@end

NS_ASSUME_NONNULL_END
