//
//  BssGoodie.h
//  BssBaseKit
//
//  Created by Baumgartner Mario on 10.06.21.
//

#import <Foundation/Foundation.h>
#import "BssMonetaryAmount.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssGoodie : BssJSONModel
@property (strong, nonatomic, nonnull) BssMonetaryAmount * redeemablePoints;
@property (strong, nonatomic, nullable) NSNumber <Optional> * redemptionsPerUser;
@property (strong, nonatomic, nonnull) NSString * publicId;
@property (nonatomic, assign) BOOL redemptionEnabled;

@end

NS_ASSUME_NONNULL_END
