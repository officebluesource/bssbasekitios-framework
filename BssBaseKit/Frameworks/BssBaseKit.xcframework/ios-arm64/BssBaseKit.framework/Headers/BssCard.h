//
//  BssCard.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BssBase.h"
#import "BssCatalogElement.h"
#import "BssCardType.h"
#import "BssItem.h"
#import "BssCardExtensions.h"

@interface BssCard : BssCatalogElement

@property (nonatomic, assign) BOOL barcodeNumberScanned;
@property (nonatomic, assign) BOOL preinstalled;

@property (nonatomic, strong, nonnull) NSString <Optional> * cardTypeId; // used in POST (will not be sent by server on GET), will never be null
@property (nonatomic, strong, nullable) NSString <Optional> * barcodeNumber;
@property (nonatomic, strong, nullable) NSString <Optional> * barcodeType;
@property (nonatomic, strong, nullable) NSString <Optional> * cardNumber;
@property (nonatomic, strong, nullable) NSString <Optional> * securityExtensionInfo;
@property (nonatomic, strong, nonnull) NSString * status;
@property (nonatomic, strong, nullable) NSString <Optional> * deactivationMessage;
@property (nonatomic, strong, nullable) NSNumber <Optional> * indexId DEPRECATED_ATTRIBUTE;

@property (nonatomic, strong, nonnull) BssCardType * cardType;
@property (nonatomic, strong, nullable) BssImage <Optional> * customFrontImage;
@property (nonatomic, strong, nullable) BssCardExtensions <Optional> * extensions;

#pragma mark - Non-Synced Properties
@property (nonatomic, strong, nullable, readonly) NSString <Ignore> * barcodeImageId;

@property (nonatomic, strong, nullable) UIImage <Ignore> * frontImageForUpload;
@property (nonatomic, strong, nullable) UIImage <Ignore> * backImageForUpload;

#pragma mark - Additions & Helper
+ (nonnull BssCard *)cardWithCardType:(nonnull BssCardType *)cardType;

- (BOOL)hasName;
- (BOOL)hasCardNumber;
- (BOOL)hasExtensions;
- (BOOL)hasChaChingExtension;
- (BOOL)cardNumberVisibilityIsVisible;
- (BOOL)cardNumberVisibilityIsObfuscated;
- (BOOL)hasBarcodeNumber;
- (BOOL)barcodeSquared;
- (BOOL)activated;
- (BOOL)isStoredBarcodeImageAvailable;

- (nullable UIImage *)displayedbarcodeImage;
- (nullable UIImage *)displayedbarcodeImageWithMaxSize:(CGSize)maxSize resultInfo:(void (^ _Nullable)(BOOL consideredQuietZone, int scaleFactor, UIImage * _Nullable barcodeImage))resultInfo;

- (nullable NSString *)displayedBarcodeNumber;
- (nullable NSString *)displayedBarcodeType;
- (nullable NSString *)displayedCardNumber;

- (void)loadBarcodeImageWithSuccess:(nullable BssEmptyBlock)success failure:(nullable BssFailureBlock)failure;
- (void)loadImagesWithCompletionBlock:(nullable BssFailureBlock)completed;

#pragma mark - Database features
// save and update card in core data. If no entity is created yet, it will be. All properties (also the ignored ones) will be overwritten. Attention: All synced properties will be overwritten after the next server sync again.
- (void)save;
// update a specific card property in coredata. If the card is not found in database, NO will be returned. If the propery cannot be mapped, an exception will be thrown.
// It is recommended to use it via NSStringFromSelector like [card savePropertyWithName:NSStringFromSelector(@selector(barcodeNumber))];
- (void)savePropertyWithName:(nonnull NSString *)propertyName;
// update card properties with core data card properties. If the card is not found in database, NO will be returned. Nothing will be stored in core data.
- (BOOL)updateFromDatabase;
// update card properties with the properties of the given card. Nothing will be stored in core data.
- (void)updateWithCard:(nonnull BssCard *)card;
// delete Card in core data. Be careful if the cards are synced with the hub. If the card is not found in database, NO will be returned.
- (void)deleteCard;
// delete images on disk and image paths (front & back).
- (void)deleteCardImagesExceptBarcodeImage;
// delete all card related images on disk and image paths (front, back and barcode).
- (void)deleteCardImages;

@end
