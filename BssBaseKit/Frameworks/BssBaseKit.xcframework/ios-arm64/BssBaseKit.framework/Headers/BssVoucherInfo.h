//
//  BssVoucherInfo.h
//  BssBaseKit
//
//  Created by Florian Hager on 13.03.18.
//  Copyright © 2018 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

typedef enum : NSUInteger {
    BssVoucherInfoLocationTypeUnknown,
    BssVoucherInfoLocationTypeInStore,
    BssVoucherInfoLocationTypeOnline,
    BssVoucherInfoLocationTypePhone
} BssVoucherInfoLocationType;

@interface BssVoucherInfo : BssJSONModel

@property (strong, nonatomic, nullable) NSNumber <Optional> * redemptionsPerUser;
@property (strong, nonatomic, nullable) NSString <Optional> * redemptionLocation;

- (BssVoucherInfoLocationType)redemptionLocationEnum;

@end
