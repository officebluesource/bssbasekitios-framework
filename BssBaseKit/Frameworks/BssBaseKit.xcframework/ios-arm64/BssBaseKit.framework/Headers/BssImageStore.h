//
//  BssImageStore.h
//  BssBaseKit
//
//  Created by Hager Florian on 11.05.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    /// The image wasn't available in the cache, but was downloaded from the web.
    BssImageStorageTypeNone,
    /// The image was obtained from the persistent store.
    BssImageStorageTypePersistentStore,
    /// The image was obtained from the cache.
    BssImageStorageTypeCache
} BssImageStorageType;

@interface BssImageStore : NSObject

/*!
 * @discussion Asynchronous call to get an image from the hub. If this image is already cached or persisted and the reload param is set to false, this image will be returned.
 * @param urlString Relative or absolut url string to the image.
 * @param authRequired Flag if an authorization header is required for this image.
 * @param reload Flag that inidicates if the image should be reloaded from the server. If set to yes, cache an persisted store will be ignored and the persisted image will be deleted.
 * @param persist Flag that indicates whether the image will be persisted.
 * @param completed Block which is called after the webservice request has been finished. In case of an success, it contains the result image, storageType and the image url. In case of an error, the error property will be set and the image property will be nil.
 */
+ (void)imageWithUrlString:(NSString * _Nonnull)urlString authRequired:(BOOL)authRequired reload:(BOOL)reload persist:(BOOL)persist completed:(void (^_Nullable)(UIImage * _Nullable image, NSError * _Nullable error, BssImageStorageType storageType, NSURL * _Nullable imageURL))completed;

/// Get stored image or nil if not found. Searches first in cache, then in persisted store.
+ (UIImage * _Nullable)imageWithUrlString:(NSString * _Nonnull)urlString;

/// Delete image with given urlstring.
/// Images of persisted models (cards, items) will be deleted automatically.
+ (BOOL)deletePersistedImageWithUrlString:(NSString * _Nonnull)urlString;

/// The key of the possibly persisted image (filename). This function only returns the generated key for the given url and is not an indicator whether the image is persisted.
+ (NSString *)keyOfPersistedImageWithUrlString:(NSString *)urlString;

/// Remove all images in all stores.
+ (void)clear;

/// Set a path where the images will be stored and searched for. Can be reset to default if set to nil.
+ (void)setPathForPersistedImages:(NSString * _Nullable)path;

@end

NS_ASSUME_NONNULL_END
