//
//  UITableView+BssAnimations.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.09.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BssBase.h"

typedef enum {
    BssUITableViewAnimationOptionFadeInNone,
    BssUITableViewAnimationOptionFadeOutNone,
    BssUITableViewAnimationOptionFadeOutVeryShort,
    BssUITableViewAnimationOptionFadeInVeryShort,
    BssUITableViewAnimationOptionFadeOutShort,
    BssUITableViewAnimationOptionFadeInShort,
    BssUITableViewAnimationOptionLinearShort,
    BssUITableViewAnimationOptionLinearLong
} BssUITableViewAnimationOption;

@interface UITableView (BssAnimations)

/**
 * Animation where the whole tableview fades first out, then in.
 * Default AnimationOption is UITableViewAnimationOptionLinearLong.
 */
- (void)BSS_reloadDataAnimated:(BOOL)animated;

/**
 * Animation where the whole tableview fades first out, then in.
 */
- (void)BSS_reloadDataAnimated:(BOOL)animated withAnimationOption:(BssUITableViewAnimationOption)animationOption completed:(BssEmptyBlock)completed;

/**
 * Animation where the whole tableview fades first out, then in.
 */
- (void)BSS_reloadDataAnimated:(BOOL)animated withFadeInDuration:(CGFloat)fadeInDuration fadeOutDuration:(CGFloat)fadeOutDuration completed:(BssEmptyBlock)completed;

@end
