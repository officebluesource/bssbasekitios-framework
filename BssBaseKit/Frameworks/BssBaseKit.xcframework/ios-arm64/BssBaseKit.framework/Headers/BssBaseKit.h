//
//  BssBaseKit.h
//  BssBaseKit
//
//  Created by Florian Hager on 12.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

//! Project version number for BssBaseKit.
FOUNDATION_EXPORT double BssBaseVersionNumber;

//! Project version string for BssBaseKit.
FOUNDATION_EXPORT const unsigned char BssBaseVersionString[];

#import "BssBase.h"
#import "BssDefines.h"

// DTOs
#import "BssBarcodePreview.h"
#import "BssCard.h"
#import "BssCardExtensions.h"
#import "BssCardExtensionChaChing.h"
#import "BssCardBonusProgram.h"
#import "BssCardType.h"
#import "BssCardTypeRequestConfirmation.h"
#import "BssCardTypeRequestInfo.h"
#import "BssCardUser.h"
#import "BssCardUserBalances.h"
#import "BssClientConfiguration.h"
#import "BssClientSettings.h"
#import "BssColorScheme.h"
#import "BssContact.h"
#import "BssContentElement.h"
#import "BssCountry.h"
#import "BssCreateCardContainer.h"
#import "BssFieldDescription.h"
#import "BssGeoLocation.h"
#import "BssImage.h"
#import "BssIssuer.h"
#import "BssItem.h"
#import "BssItemFilters.h"
#import "BssItemInfo.h"
#import "BssItemLocation.h"
#import "BssItemOrder.h"
#import "BssRedemptionType.h"
#import "BssRedemptionInformation.h"
#import "BssRedeemedCouponResponse.h"
#import "BssVoucherInfo.h"
#import "BssItemDisplayedLog.h"
#import "BssLog.h"
#import "BssRetailer.h"
#import "BssRetailerHints.h"
#import "BssRetailerStore.h"
#import "BssSecurityExtensionDescription.h"
#import "BssSelectedCountry.h"
#import "BssTag.h"
#import "BssReceiptProduct.h"
#import "BssReceiptDetails.h"
#import "BssReceiptAddress.h"
#import "BssReceiptFooterContent.h"
#import "BssReceipt.h"
#import "BssReceiptFavorite.h"
#import "BssReceiptStub.h"
#import "BssMonetaryAmount.h"
#import "BssChaChingTransactionPosition.h"
#import "BssChaChingTransaction.h"
#import "BssChaChingTransactionAttributes.h"
#import "BssChaChingTransactionSummary.h"
#import "BssExtensionBalance.h"
#import "BssExtensionStatus.h"
#import "BssLoyaltyPromotion.h"
#import "BssLoyaltyCouponCode.h"
#import "BssLottery.h"
#import "BssLotteryLink.h"
#import "BssOfflineLotteryOptions.h"
#import "BssLinkResource.h"
#import "BssLotteryParticipantResource.h"
#import "BssGoodie.h"
#import "BssCPCardType.h"
#import "BssCPRetailer.h"
#import "BssCPGoodiePreview.h"
#import "BssCPRedemption.h"
#import "BssCurrenciesInfoModel.h"
#import "BssCurrenciesContributions.h"
#import "BssCurrencyInfoModel.h"
#import "BssCatalogElementType.h"
#import "BssInsuranceType.h"
#import "BssCatalogElement.h"
#import "BssCatalogElementImageDownloadInfo.h"
#import "BssCatalogElementTypeEnumClass.h"
#import "BssCreateInsurance.h"
#import "BssUpdateInsurance.h"
#import "BssInsurance.h"
#import "BssInsuranceKind.h"
#import "BssContactPerson.h"

// Database
#import "BssDatabase.h"

// Webservice
#import "BssWebserviceClient.h"
#import "BssAuthManager.h"

// Image
#import "BssImageUploader.h"
#import "BssImageStore.h"

// GUI
#import "BssBadgeLabel.h"
#import "BssColoredButton.h"
#import "BssLoadableImageView.h"

// Utils
#import "BssAlertController.h"
#import "BssUtils.h"
#import "BssSecurityExtensionManager.h"
#import "BssLogUtils.h"

// Categories (Additions)
#import "NSData+BssCrypto.h"
#import "NSDateFormatter+BssAdditions.h"
#import "NSError+BssWebservice.h"
#import "NSString+BssUtils.h"
#import "UICollectionView+BssAnimations.h"
#import "UIColor+BssAdditions.h"
#import "UIImage+BssAdditions.h"
#import "UITableView+BssAnimations.h"
#import "UIView+BssAdditions.h"
#import "UIViewController+BssAdditions.h"

// Libraries
#import "BssJSONModelLib.h"
