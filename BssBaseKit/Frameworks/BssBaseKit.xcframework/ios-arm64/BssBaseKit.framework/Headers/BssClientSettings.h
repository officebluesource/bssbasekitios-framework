//
//  BssClientSettings.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssBase.h"

@interface BssClientSettings : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * key;
@property (strong, nonatomic, nonnull) NSString * value;

@property (strong, nonatomic, nonnull) NSDate * lastUpdate;
@property (strong, nonatomic, nullable) NSDate <Ignore> * lastUpdateLocal;

#pragma mark - Database features
- (void)save;

@end
