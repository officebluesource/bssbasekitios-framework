//
//  NSString+BssUtils.h
//  BssBaseKit
//
//  Created by Florian Hager on 12.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (BssUtils)

- (BOOL)BSS_isValidEmailFormat;
- (BOOL)BSS_isEmpty;
- (BOOL)BSS_isPhoneNumber;

- (NSString *)BSS_stringByAppendingQueryItems:(NSArray<NSURLQueryItem *> *)queryItems;
- (NSString *)BSS_stripHtml;

+ (NSString *)BSS_hexStringFromColor:(UIColor *)color;

- (BOOL)BSS_isRootUrlOnly;

@end
