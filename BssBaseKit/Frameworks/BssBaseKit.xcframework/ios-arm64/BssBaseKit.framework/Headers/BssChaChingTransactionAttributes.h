//
//  BssChaChingTransactionAttributes.h
//  BssBaseKit
//
//  Created by Hager Florian on 25.06.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssMonetaryAmount.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssChaChingTransactionAttributes : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * descr;
@property (strong, nonatomic, nonnull) BssMonetaryAmount * amount;

@end

NS_ASSUME_NONNULL_END

