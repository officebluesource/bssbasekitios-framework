//
//  BssJSONKeyMapper.h
//  BssJSONModel
//

#import <Foundation/Foundation.h>

typedef NSString *(^BssJSONModelKeyMapBlock)(NSString *keyName);

/**
 * **You won't need to create or store instances of this class yourself.** If you want your model
 * to have different property names than the BssJSON feed keys, look below on how to
 * make your model use a key mapper.
 *
 * For example if you consume BssJSON from twitter
 * you get back underscore_case style key names. For example:
 *
 * <pre>"profile_sidebar_border_color": "0094C2",
 * "profile_background_tile": false,</pre>
 *
 * To comply with Obj-C accepted camelCase property naming for your classes,
 * you need to provide mapping between BssJSON keys and ObjC property names.
 *
 * In your model overwrite the + (BssJSONKeyMapper *)keyMapper method and provide a BssJSONKeyMapper
 * instance to convert the key names for your model.
 *
 * If you need custom mapping it's as easy as:
 * <pre>
 * + (BssJSONKeyMapper *)keyMapper {
 * &nbsp; return [[BssJSONKeyMapper&nbsp;alloc]&nbsp;initWithDictionary:@{@"crazy_JSON_name":@"myCamelCaseName"}];
 * }
 * </pre>
 * In case you want to handle underscore_case, **use the predefined key mapper**, like so:
 * <pre>
 * + (BssJSONKeyMapper *)keyMapper {
 * &nbsp; return [BssJSONKeyMapper&nbsp;mapperFromUnderscoreCaseToCamelCase];
 * }
 * </pre>
 */
@interface BssJSONKeyMapper : NSObject

// deprecated
@property (readonly, nonatomic) BssJSONModelKeyMapBlock BssJSONToModelKeyBlock DEPRECATED_ATTRIBUTE;
- (NSString *)convertValue:(NSString *)value isImportingToModel:(BOOL)importing DEPRECATED_MSG_ATTRIBUTE("use convertValue:");
- (instancetype)initWithDictionary:(NSDictionary *)map DEPRECATED_MSG_ATTRIBUTE("use initWithModelToJSONDictionary:");
- (instancetype)initWithJSONToModelBlock:(BssJSONModelKeyMapBlock)toModel modelToJSONBlock:(BssJSONModelKeyMapBlock)toJSON DEPRECATED_MSG_ATTRIBUTE("use initWithModelToJSONBlock:");
+ (instancetype)mapper:(BssJSONKeyMapper *)baseKeyMapper withExceptions:(NSDictionary *)exceptions DEPRECATED_MSG_ATTRIBUTE("use baseMapper:withModelToJSONExceptions:");
+ (instancetype)mapperFromUnderscoreCaseToCamelCase DEPRECATED_MSG_ATTRIBUTE("use mapperForSnakeCase:");
+ (instancetype)mapperFromUpperCaseToLowerCase DEPRECATED_ATTRIBUTE;

/** @name Name converters */
/** Block, which takes in a property name and converts it to the corresponding BssJSON key name */
@property (readonly, nonatomic) BssJSONModelKeyMapBlock modelToJSONKeyBlock;

/** Combined converter method
 * @param value the source name
 * @return BssJSONKeyMapper instance
 */
- (NSString *)convertValue:(NSString *)value;

/** @name Creating a key mapper */

/**
 * Creates a BssJSONKeyMapper instance, based on the block you provide this initializer.
 * The parameter takes in a BssJSONModelKeyMapBlock block:
 * <pre>NSString *(^BssJSONModelKeyMapBlock)(NSString *keyName)</pre>
 * The block takes in a string and returns the transformed (if at all) string.
 * @param toJSON transforms your model property name to a BssJSON key
 */
- (instancetype)initWithModelToJSONBlock:(BssJSONModelKeyMapBlock)toJSON;

/**
 * Creates a BssJSONKeyMapper instance, based on the mapping you provide.
 * Use your BssJSONModel property names as keys, and the BssJSON key names as values.
 * @param toJSON map dictionary, in the format: <pre>@{@"myCamelCaseName":@"crazy_JSON_name"}</pre>
 * @return BssJSONKeyMapper instance
 */
- (instancetype)initWithModelToJSONDictionary:(NSDictionary *)toJSON;

/**
 * Given a camelCase model property, this mapper finds BssJSON keys using the snake_case equivalent.
 */
+ (instancetype)mapperForSnakeCase;

/**
 * Given a camelCase model property, this mapper finds BssJSON keys using the TitleCase equivalent.
 */
+ (instancetype)mapperForTitleCase;

/**
 * Creates a BssJSONKeyMapper based on a built-in BssJSONKeyMapper, with specific exceptions.
 * Use your BssJSONModel property names as keys, and the BssJSON key names as values.
 */
+ (instancetype)baseMapper:(BssJSONKeyMapper *)baseKeyMapper withModelToJSONExceptions:(NSDictionary *)toJSON;

@end
