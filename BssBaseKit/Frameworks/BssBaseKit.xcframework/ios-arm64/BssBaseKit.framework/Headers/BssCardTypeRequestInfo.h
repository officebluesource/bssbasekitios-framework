//
//  BssCardTypeRequestInfoDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssFieldDescription.h"
#import "BssImage.h"

@protocol BssFieldDescription;

@interface BssCardTypeRequestInfo : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * requestToken;
@property (strong, nonatomic, nullable) NSString <Optional> * barcodeNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * cardNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * passwordEncryptionPublicKey;
@property (strong, nonatomic, nonnull) NSString * termsAndConditionsUrl;
@property (strong, nonatomic, nonnull) NSString * advantagesSummary;
@property (strong, nonatomic, nonnull) NSString * advantages;

@property (strong, nonatomic, nonnull) BssImage * logo;
@property (strong, nonatomic, nonnull) BssImage * image;

@property (strong, nonatomic, nonnull) NSArray<BssFieldDescription *> <BssFieldDescription> * fields;

@property (nonatomic) NSInteger minimumAge;

@end
