//
//  UIColor+BssAdditions.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.09.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BssAdditions)

- (BOOL)BSS_isColorDark;
+ (UIColor *)BSS_colorFromHexString:(NSString *)hexString; // returns nil if hexString is nil
+ (UIColor *)BSS_primaryColor100;
+ (UIColor *)BSS_primaryColor25;
+ (void)BSS_setPrimaryColor:(UIColor *)color;

@end
