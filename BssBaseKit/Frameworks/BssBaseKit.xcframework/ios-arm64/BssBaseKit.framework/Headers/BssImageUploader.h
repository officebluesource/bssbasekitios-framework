//
//  MPImageUploader.h
//  mobile-pocket
//
//  Created by Florian Hager on 03.06.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssBase.h"

@interface BssImageUploadOperation : NSObject
@property (strong, nonatomic) UIImage* imagetoUpload;
@property (strong, nonatomic) NSObject* targetObject;
@property (strong, nonatomic) NSString* targetProperty;
@end

@interface BssImageUploader : NSObject

- (void)uploadImages:(NSArray<BssImageUploadOperation *> *)operationQueue success:(BssEmptyBlock)success failure:(BssFailureBlock)failure;

@end
