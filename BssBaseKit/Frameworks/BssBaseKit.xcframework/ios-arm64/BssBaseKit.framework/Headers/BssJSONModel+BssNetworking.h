//
//  BssJSONModel+networking.h
//  BssJSONModel
//

#import "BssJSONModel.h"
#import "BssJSONHTTPClient.h"

typedef void (^BssJSONModelBlock)(id model, BssJSONModelError *err) DEPRECATED_ATTRIBUTE;

@interface BssJSONModel (BssNetworking)

- (instancetype)BSS_initFromURLWithString:(NSString *)urlString completion:(BssJSONModelBlock)completeBlock DEPRECATED_ATTRIBUTE;
+ (void)BSS_getModelFromURLWithString:(NSString *)urlString completion:(BssJSONModelBlock)completeBlock DEPRECATED_ATTRIBUTE;
+ (void)BSS_postModel:(BssJSONModel *)post toURLWithString:(NSString *)urlString completion:(BssJSONModelBlock)completeBlock DEPRECATED_ATTRIBUTE;

@end
