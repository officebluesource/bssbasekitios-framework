//
//  BssReceiptProduct.h
//  BssLoyalty
//
//  Created by Viacheslav Korkhonen on 08.03.19.
//  Copyright © 2019 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

#define BSS_WARRANTY_END_DATE @"WARRANTY_END_DATE"
#define BSS_WARRANTY_INFO_TEXT @"WARRANTY_INFO_TEXT"

NS_ASSUME_NONNULL_BEGIN

@interface BssReceiptProduct : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * uniqueId;
@property (strong, nonatomic, nullable) NSString <Optional> * name;
@property (strong, nonatomic, nullable) NSString <Optional> * taxClass;
@property (strong, nonatomic, nullable) NSString <Optional> * warrantyEndDate;
@property (strong, nonatomic, nullable) NSString <Optional> * warrantyInfoText;
@property (strong, nonatomic, nullable) NSString <Optional> * warrantyInfoType;

@property (strong, nonatomic, nullable) NSNumber <Optional> * price;
@property (strong, nonatomic, nullable) NSNumber <Optional> * quantity;
@property (strong, nonatomic, nullable) NSNumber <Optional> * warrantyValidForDays;

@property BOOL promotion;


#pragma mark - Helpers
- (BOOL)hasWarranty;
- (nullable NSDate *)warrantyEndDateAsNSDate;

@end

NS_ASSUME_NONNULL_END
