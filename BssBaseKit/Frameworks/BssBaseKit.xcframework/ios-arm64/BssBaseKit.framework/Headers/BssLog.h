//
//  BssLog.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssLog : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * action;
@property (strong, nonatomic, nonnull) NSString * loggedObjectId;

@property (strong, nonatomic, nonnull) NSDate * timestamp;

@end
