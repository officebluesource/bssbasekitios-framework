//
//  BssLotteryParticipantAcceptedLink.h
//  BssBaseKit
//
//  Created by Hager Florian on 22.02.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssLinkResource : BssJSONModel

@property (nonatomic, assign) int32_t uniqueId;

@end

NS_ASSUME_NONNULL_END
