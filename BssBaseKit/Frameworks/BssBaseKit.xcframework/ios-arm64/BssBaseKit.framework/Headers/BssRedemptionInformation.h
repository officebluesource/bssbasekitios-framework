//
//  BssItemVoucherDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssRedemptionType.h"

@interface BssRedemptionInformation : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * redemptionType;
@property (nonatomic, assign) BOOL redeemed;
@property (strong, nonatomic, nullable) NSDate * redeemableUntilDate;

@property (strong, nonatomic, nullable) NSNumber <Optional> * remainingRedemptions;

- (BssRedemptionTypeEnum)redemptionTypeEnum;

@end
