//
//  BssLoadableImageView.h
//  BssBaseKit
//
//  Created by Florian Hager on 18.09.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BssBase.h"
#import "BssImageStore.h"
#import "BssCard.h"
#import "BssInsurance.h"

OBJC_EXPORT double const kBssLoadableImageViewLoadingViewsFadeOutValue;
OBJC_EXPORT double const kBssLoadableImageViewBarcodeFadeInValue;

@interface BssLoadableImageView : UIImageView

@property (strong, nonatomic) UIView * _Nullable loadingColorView;
@property (strong, nonatomic) UIImageView * _Nullable loadingImageView;

#pragma mark - Helper
/**
 * @discussion Sets the given image and calls removePlaceholderAnimated:NO.
 * @param image The given image which will be set.
 */
- (void)setImageAndHideLoadingViews:(UIImage * _Nullable)image;

/**
 * @discussion Fade out all loading views.
 * @param animated If set to YES, the loading views will be faded out with the kBssLoadableImageViewLoadingViewsFadeOutValue value.
 */
- (void)removeLoadingViewsAnimated:(BOOL)animated;

#pragma mark - Barcode Image
- (void)setBarcodeImageWithUri:(NSString * _Nonnull)barcodeUri reloadIfCached:(BOOL)reloadIfCached indexPath:(NSIndexPath * _Nullable)indexPath barcodeType:(NSString * _Nonnull)barcodeType downloadCompleted:(void (^ _Nullable)(UIImage * _Nullable barcodeImage, NSString * _Nullable barcodeType, NSError * _Nullable error, NSIndexPath * _Nullable givenIndexPath))completionBlock;


#pragma mark - Image
/**
 * @discussion Async image download which can be used to set the downloaded image for the given imageview. Placeholder will be hidden animated if the image have not been cached yet.
 * @param imageUrlString The urlString of the desired image.
 * @param authRequired Flag whether the authentication header should be used or not.
 * @param loadingColor Color which will be shown until the downloaded image is set. Modify properties of its containerview via the loadingColorView property. For hex strings, use +[UIColor BSS_colorFromHexString:].
 * @param loadingImage Image which will be shown until the downloaded image is set. Modify properties of its containerview via the loadingImageView property.
 * @param reloadIfCached If set to yes, the image will be downloaded whether it is cached.
 */
- (void)setImageWithUrlString:(NSString * _Nullable)imageUrlString authRequired:(BOOL)authRequired loadingColor:(UIColor * _Nullable)loadingColor loadingImage:(UIImage * _Nullable)loadingImage reloadIfCached:(BOOL)reloadIfCached;

/**
 * @discussion Async image download which can be used to set the downloaded image for the given imageview. If setImageAutomatically is true, placeholder will be hidden animated if the image have not been cached yet.
 * @param imageUrlString The urlString of the desired image.
 * @param authRequired Flag whether the authentication header should be used or not.
 * @param loadingColor Color which will be shown until the downloaded image is set. Modify properties of its containerview via the loadingColorView property. For hex strings, use +[UIColor BSS_colorFromHexString:].
 * @param loadingImage Image which will be shown until the downloaded image is set. Modify properties of its containerview via the loadingImageView property.
 * @param reloadIfCached If set to yes, the image will be downloaded whether it is cached.
 * @param setImageAutomatically Use NO for scrollable listview like tableviews or collectionviews where async imagesdownload may complete when the cell isn't visible any more. Take care to check if image is not null and if cell is still visible.
 * @param indexPath the current indexpath
 * @param completionBlock Completionblock which returns the image, error, cachetype, imageurl and given indexpath.
 */
- (void)setImageWithUrlString:(NSString * _Nullable)imageUrlString authRequired:(BOOL)authRequired loadingColor:(UIColor * _Nullable)loadingColor loadingImage:(UIImage * _Nullable)loadingImage reloadIfCached:(BOOL)reloadIfCached setImageAutomatically:(BOOL)setImageAutomatically indexPath:(NSIndexPath * _Nullable)indexPath downloadCompleted:(void (^ _Nullable)(UIImage * _Nullable image, NSError * _Nullable error, BssImageStorageType storageType, NSURL * _Nullable imageURL, NSIndexPath * _Nullable givenIndexPath))completionBlock;


/**
 * @discussion Async Image donwload for a specific side of a catalogElement, which considers custom images. If image is already stored to disk an shall not be reloaded, this image is taken.
 * Image will be set automatically. The card will not be updated in the database.
 * If image has to be downloaded, the current screewidth will be taken as parameter.
 * @param catalogElement The exisiting catalogElement for which the image should be loaded.
 * @param frontSide Flag which indicates for which side of the card the image should be loaded.
 * @param reloadImage If set to yes, the image will be downloaded whether it is cached or already stored to disk.
 * @param showLoadingColor Flag which indicates whether the loading color of the card image should be shown or not. Modify properties of its containerview via the loadingColorView property.
 * @param loadingImage Image which will be shown until the downloaded image is set. Modify properties of its containerview via the loadingImageView property.
 * @param completionBlock Completionblock which returns the image, error, cachetype and imageurl.
 */
- (void)setImageForCatalogElement:(BssCatalogElement * _Nonnull)catalogElement frontSide:(BOOL)frontSide reloadImage:(BOOL)reloadImage persist:(BOOL)persist showLoadingColor:(BOOL)showLoadingColor loadingImage:(UIImage * _Nullable)loadingImage completionBlock:(void (^ _Nullable)(UIImage * _Nullable image, NSError * _Nullable error, BssImageStorageType storageType, NSURL * _Nullable imageURL))completionBlock;

/**
 * @discussion Async Image donwload for a specific side of a card, which considers custom images. If image is already stored to disk an shall not be reloaded, this image is taken.
 * Image will be set automatically. The card will not be updated in the database.
 * If image has to be downloaded, the current screewidth will be taken as parameter.
 * @param card The exisiting card for which the image should be loaded.
 * @param frontSide Flag which indicates for which side of the card the image should be loaded.
 * @param reloadImage If set to yes, the image will be downloaded whether it is cached or already stored to disk.
 * @param showLoadingColor Flag which indicates whether the loading color of the card image should be shown or not. Modify properties of its containerview via the loadingColorView property.
 * @param loadingImage Image which will be shown until the downloaded image is set. Modify properties of its containerview via the loadingImageView property.
 * @param completionBlock Completionblock which returns the image, error, cachetype and imageurl.
 */
- (void)setImageForCard:(BssCard * _Nonnull)card frontSide:(BOOL)frontSide reloadImage:(BOOL)reloadImage persist:(BOOL)persist showLoadingColor:(BOOL)showLoadingColor loadingImage:(UIImage * _Nullable)loadingImage completionBlock:(void (^ _Nullable)(UIImage * _Nullable image, NSError * _Nullable error, BssImageStorageType storageType, NSURL * _Nullable imageURL))completionBlock;

/**
 * @discussion Async Image donwload for a specific side of an insurance, which considers custom images. If image is already stored to disk an shall not be reloaded, this image is taken.
 * Image will be set automatically. The card will not be updated in the database.
 * If image has to be downloaded, the current screewidth will be taken as parameter.
 * @param insurance The exisiting insurance for which the image should be loaded.
 * @param frontSide Flag which indicates for which side of the card the image should be loaded.
 * @param reloadImage If set to yes, the image will be downloaded whether it is cached or already stored to disk.
 * @param showLoadingColor Flag which indicates whether the loading color of the card image should be shown or not. Modify properties of its containerview via the loadingColorView property.
 * @param loadingImage Image which will be shown until the downloaded image is set. Modify properties of its containerview via the loadingImageView property.
 * @param completionBlock Completionblock which returns the image, error, cachetype and imageurl.
 */
- (void)setImageForInsurance:(BssInsurance * _Nonnull)insurance frontSide:(BOOL)frontSide reloadImage:(BOOL)reloadImage persist:(BOOL)persist showLoadingColor:(BOOL)showLoadingColor loadingImage:(UIImage * _Nullable)loadingImage completionBlock:(void (^ _Nullable)(UIImage * _Nullable image, NSError * _Nullable error, BssImageStorageType storageType, NSURL * _Nullable imageURL))completionBlock;

@end
