//
//  LoginCredentialsEntity.h
//  mobile-pocket
//
//  Created by Florian Hager on 05.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "BssRegisterCredentials.h"
#import "BssJSONModel.h"

@interface BssLoginCredentials : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * type;
@property (strong, nonatomic, nonnull) NSString * mobileClientId;
@property (strong, nonatomic, nullable) NSString <Optional> * user;
@property (strong, nonatomic, nullable) NSString <Optional> * password;
@property (strong, nonatomic, nullable) NSString <Optional> * token;

@end
