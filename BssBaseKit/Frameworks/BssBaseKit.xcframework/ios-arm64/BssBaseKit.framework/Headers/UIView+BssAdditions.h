//
//  UIView+BssAdditions.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.09.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BssAdditions)

#pragma mark - Nib Loading
+ (instancetype)BSS_loadNibNamed:(NSString *)nibName;
+ (instancetype)BSS_loadNibNamed:(NSString *)nibName bundle:(NSBundle *)bundle;

#pragma mark - Pixel Calculation
+ (float)BSS_pixelToPhysicalPixel:(float)pixel;

#pragma mark - Animations
- (void)BSS_removeFromSuperviewAnimated:(BOOL)animated;
- (void)BSS_startShakeAnimationWithDuration:(CGFloat)duration fromTiltValue:(CGFloat)fromTiltValue toTiltValue:(CGFloat)toTiltValue repeatCount:(NSUInteger)repeatCount shakeHorizontal:(BOOL)shakeHorizontal delegate:(id<CAAnimationDelegate>)delegate;
- (void)BSS_startShakeAnimationWithDuration:(CGFloat)duration fromTiltXValue:(CGFloat)fromTiltXValue toTiltXValue:(CGFloat)toTiltXValue fromTiltYValue:(CGFloat)fromTiltYValue toTiltYValue:(CGFloat)toTiltYValue repeatCount:(NSUInteger)repeatCount delegate:(id<CAAnimationDelegate>)delegate;
- (void)BSS_addParallaxEffectWithTiltValue:(NSInteger)tiltValue;

@end
