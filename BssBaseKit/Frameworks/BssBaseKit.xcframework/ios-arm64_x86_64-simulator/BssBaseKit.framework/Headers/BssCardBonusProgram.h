//
//  BssCardBonusProgramDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssCardBonusProgram : BssJSONModel

// The bonus program information as plain text
@property (strong, nonatomic, nonnull) NSString * message;

@end
