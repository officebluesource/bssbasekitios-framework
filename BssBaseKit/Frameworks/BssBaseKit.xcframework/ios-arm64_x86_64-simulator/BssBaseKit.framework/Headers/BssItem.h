//
//  BssItemDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssImage.h"
#import "BssIssuer.h"
#import "BssTag.h"
#import "BssContentElement.h"
#import "BssItemLocation.h"
#import "BssVoucherInfo.h"
#import "BssLottery.h"
#import "BssBase.h"
#import "BssGoodie.h"

@protocol BssContentElement, BssTag, BssItemLocation;

typedef enum {
    BssItemTypeUnknown,
    BssItemTypeOffer,
    BssItemTypeCoupon,
    BssItemTypeEvent,
    BssItemTypeInfo,
    BssItemTypeTicket,
    BssItemTypeOther,
    BssItemTypeOfflineLottery,
    BssItemTypeGoodie
} BssItemType;

typedef enum {
    BssItemImportanceUnknown,
    BssItemImportanceLow,
    BssItemImportanceMedium,
    BssItemImportanceHigh
} BssItemImportance;

@interface BssItem : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * subHeadline;
@property (strong, nonatomic, nullable) NSString <Optional> * descr;
@property (strong, nonatomic, nullable) NSString <Optional> * primaryContentElementId;
@property (strong, nonatomic, nonnull) NSString * uniqueId;
@property (strong, nonatomic, nonnull) NSString * type;
@property (strong, nonatomic, nonnull) NSString * headline;
@property (strong, nonatomic, nonnull) NSString * retailerId;
@property (strong, nonatomic, nonnull) NSString * importance;

@property (strong, nonatomic, nonnull) NSDate * startDate;
@property (strong, nonatomic, nonnull) NSDate * endDate;

@property (strong, nonatomic, nonnull) BssImage * image;
@property (strong, nonatomic, nullable) BssImage <Optional> * optionalGifImage;
@property (strong, nonatomic, nullable) BssIssuer <Optional> * issuer;
@property (strong, nonatomic, nullable) BssVoucherInfo <Optional> * voucher;
@property (strong, nonatomic, nullable) BssLottery <Optional> * lottery;
@property (strong, nonatomic, nullable) BssGoodie <Optional> * goodie;

@property (strong, nonatomic, nullable) NSArray<BssContentElement *> <BssContentElement, Optional> * contentElements;
@property (strong, nonatomic, nullable) NSArray<BssTag *> <BssTag, Optional> * tags;
@property (strong, nonatomic, nullable) NSArray<BssItemLocation *> <BssItemLocation, Optional> * locations;

- (BssItemType)typeEnum;
- (BssItemImportance)importanceEnum;
- (BOOL)hasOptionalGifImage;

#pragma mark - Non-Synced Properties
// url that points to the urlLarge property of the image property (BssImage).
@property (strong, nonatomic, readonly, nonnull) NSString <Ignore> * imageUrl;
// url that points to the urlSmall property of the image property (BssImage).
@property (strong, nonatomic, readonly, nonnull) NSString <Ignore> * thumbnailUrl;

// Date when offline item was saved the first time.
@property (strong, nonatomic, nullable) NSDate <Ignore> * savedDate;

// Date when offline item was updated the last time.
@property (strong, nonatomic, nullable) NSDate <Ignore> * lastUpdatedDate;

// Check if item is already saved in database.
@property (nonatomic, assign, readonly, getter=isSaved) BOOL saved;

// Number of participations (lottery)
@property (nonatomic, assign) int32_t numParticipations;


#pragma mark - Database features
/*!
 * @discussion Save item in database for offline access. If the item already exists (check on uniqueId), it will be updated (locally stored properties will not be overwritten).
 * If the image is currently cached, it will be stored persistently, otherwise no image will be stored.
 */
- (void)save;

/*!
 * @discussion Save item in database for offline access. If the item already exists (check on uniqueId), it will be updated.
 * This method is async because the item image will be downloaded (if not cached) and stored.
 * The stored image will be accessable via the imagePath property.
 * Item will be saved in database no matter if the image download was successful.
 */
- (void)saveWithSuccess:(nullable BssEmptyBlock)success imageDownloadFailure:(nullable BssFailureBlock)failure;

/*!
 * @discussion Update a specific item property in coredata. If the item is not found in database, NO will be returned. If the propery cannot be mapped, an exception will be thrown.
 * It is recommended to use it via NSStringFromSelector like [item savePropertyWithName:NSStringFromSelector(@selector(imagePath))];
 */
- (void)savePropertyWithName:(nullable NSString *)propertyName;

/**
 * @discussion Check if imagePath is set and image is available on disk
 */
- (BOOL)isStoredImageAvailable;

/**
 * @discussion Check if offline offer has already expired (endDate)
 */
- (BOOL)isExpired;

/*!
 * @discussion Delete stored image on disk. ImagePath property will be updated.
 */
- (void)deleteImage;

/*!
 * @discussion Delete item in database. In case of an existing itemPath, the image will also be deleted from disk.
 */
- (void)deleteItem;

@end
