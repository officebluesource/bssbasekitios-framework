//
//  BssInsurance.h
//  BssBaseKit
//
//  Created by Hager Florian on 12.07.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssCatalogElement.h"
#import "BssInsuranceType.h"
#import "BssContactPerson.h"
#import "BssInsuranceKind.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssInsurance : BssCatalogElement

@property (nonatomic, strong, nonnull) BssInsuranceType * insuranceType;
@property (nonatomic, strong, nullable) NSString <Optional> * policyNumber;
@property (nonatomic, strong, nonnull) NSString * insuranceKind;
@property (nonatomic, strong, nullable) NSString <Optional> * customServiceHotline;
@property (nonatomic, strong, nullable) BssContactPerson <Optional> * contactPerson;

#pragma mark - Non-Synced Properties
@property (nonatomic, strong, nullable, readonly) NSString <Ignore> * frontImageUrl;
@property (nonatomic, strong, nullable, readonly) NSString <Ignore> * backImageUrl;

#pragma mark - Additions & Helper
+ (nonnull BssInsurance *)insuranceWithInsuranceType:(nonnull BssInsuranceType *)insuranceType;
- (BssInsuranceKindEnum)insuranceKindEnum;

- (void)loadImagesWithCompletionBlock:(nullable BssFailureBlock)completed;
- (nonnull NSString *)serviceHotline;

#pragma mark - Database features
/// save and update card in core data. If no entity is created yet, it will be. All properties (also the ignored ones) will be overwritten. Attention: All synced properties will be overwritten after the next server sync again.
- (void)save;
/// update a specific insurance property in coredata. If the propery cannot be mapped, an exception will be thrown.
/// It is recommended to use it via NSStringFromSelector like [insurance savePropertyWithName:NSStringFromSelector(@selector(customServiceHotline))];
- (void)savePropertyWithName:(nonnull NSString *)propertyName;
/// delete insurance in core data. Be careful if the cards are synced with the hub.
- (void)deleteInsurance;
// delete all insurance related images on disk (front, back).
- (void)deleteImages;

@end

NS_ASSUME_NONNULL_END
