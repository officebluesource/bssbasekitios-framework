//
//  RegisterCredentialsEntity.h
//  mobile-pocket
//
//  Created by Florian Hager on 05.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssRegisterCredentials : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * mobileClientId;
@property (strong, nonatomic, nonnull) NSString * mobileClientSecret;
@property (strong, nonatomic, nonnull) NSString * user;
@property (strong, nonatomic, nonnull) NSString * type;
@property (strong, nonatomic, nullable) NSString <Optional> * password;

@end
