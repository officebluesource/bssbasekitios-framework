//
//  BssReceiptStub.h
//  BssBaseKit
//
//  Created by Korkhonen Viacheslav on 10.04.19.
//  Copyright © 2019 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssReceiptStub : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * receiptNumber;
@property (strong, nonatomic, nonnull) NSString * creationDate;

@end

NS_ASSUME_NONNULL_END
