//
//  BssColorScheme.h
//  BssBaseKit
//
//  Created by Florian Hager on 21.12.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssColorScheme : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * primaryColor;

@end
