//
//  BssImage.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BssJSONModel.h"

@interface BssImage : BssJSONModel

@property (nonatomic, assign) BOOL authenticationRequired;

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;

@property (strong, nonatomic, nonnull) NSString * uniqueId;
@property (strong, nonatomic, nonnull) NSString * color;

/** Use for thumbnails or images that fit max half of the screen width */
@property (strong, nonatomic, nonnull) NSString * urlSmall;

/** Use for images that fit the screennwidth */
@property (strong, nonatomic, nonnull) NSString * urlMedium;

/** Use for images that are likely displayed fullscreen (best available quality will be downloaded) */
@property (strong, nonatomic, nonnull) NSString * urlLarge;

@end
