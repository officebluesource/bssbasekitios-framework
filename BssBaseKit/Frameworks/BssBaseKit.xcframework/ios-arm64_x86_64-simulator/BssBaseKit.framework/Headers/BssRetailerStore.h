//
//  BssRetailerStoreDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssContact.h"
#import "BssImage.h"

@interface BssRetailerStore : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * uniqueId;
@property (strong, nonatomic, nonnull) NSString * name;
@property (strong, nonatomic, nullable) NSString <Optional> * openingHours;
@property (strong, nonatomic, nullable) BssImage <Optional> * image;

@property (strong, nonatomic, nonnull) BssContact * contact;

@end
