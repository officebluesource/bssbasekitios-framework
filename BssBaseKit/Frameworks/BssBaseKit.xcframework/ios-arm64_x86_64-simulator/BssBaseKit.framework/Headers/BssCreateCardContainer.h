//
//  BssCreateCardContainerDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 17.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssCard.h"
#import "BssCardTypeRequestConfirmation.h"

@interface BssCreateCardContainer : BssJSONModel

@property (strong, nonatomic, nonnull) BssCard * cardData;
@property (strong, nonatomic, nonnull) BssCardTypeRequestConfirmation * confirmationData;

@end
