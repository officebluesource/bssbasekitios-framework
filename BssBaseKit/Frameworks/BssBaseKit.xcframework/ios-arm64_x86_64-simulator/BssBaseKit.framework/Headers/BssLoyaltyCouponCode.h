//
//  BssLoyaltyCouponCode.h
//  BssBaseKit
//
//  Created by Hager Florian on 07.12.20.
//  Copyright © 2020 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssLoyaltyCouponCode : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * value;
@property (strong, nonatomic, nullable) NSString <Optional> * type;

@end
