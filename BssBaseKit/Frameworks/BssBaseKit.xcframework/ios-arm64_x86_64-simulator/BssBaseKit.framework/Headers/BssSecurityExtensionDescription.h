//
//  BssSecurityExtensionDescription.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssSecurityExtensionDescription : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * authType;
@property (strong, nonatomic, nullable) NSString <Optional> * authInfoText;
@property (strong, nonatomic, nullable) NSString <Optional> * authHelpText;
@property (strong, nonatomic, nullable) NSString <Optional> * passwordEncryptionPublicKey;


- (BOOL)isAuthTypeNone;
- (BOOL)isAuthTypeKnown;
- (BOOL)isAuthTypeUsernamePassword;

@end
