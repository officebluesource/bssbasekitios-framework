//
//  BssReceiptFavorite.h
//  BssBaseKit
//
//  Created by Korkhonen Viacheslav on 10.04.19.
//  Copyright © 2019 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssReceiptFavorite : BssJSONModel

@property BOOL favorite;

@end

NS_ASSUME_NONNULL_END
