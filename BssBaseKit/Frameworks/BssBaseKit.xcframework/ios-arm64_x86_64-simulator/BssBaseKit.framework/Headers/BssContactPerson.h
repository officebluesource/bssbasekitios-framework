//
//  BssContactPerson.h
//  BssBaseKit
//
//  Created by Hager Florian on 12.07.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssContactPerson : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * name;
@property (strong, nonatomic, nullable) NSString <Optional> * phoneNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * emailAddress;

- (instancetype)initWithName:(NSString * _Nullable)name phoneNumber:(NSString * _Nullable)phoneNumber emailAddress:(NSString * _Nullable)emailAddress;

@end

NS_ASSUME_NONNULL_END
