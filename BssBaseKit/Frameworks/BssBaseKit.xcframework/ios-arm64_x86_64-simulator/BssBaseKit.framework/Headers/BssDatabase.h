//
//  BssDatabase.h
//  BssBaseKit
//
//  Created by Florian Hager on 05.01.18.
//  Copyright © 2018 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssBase.h"
#import "BssCard.h"
#import "BssInsurance.h"
#import "BssItem.h"
#import "BssClientConfiguration.h"
#import "BssClientSettings.h"
#import "BssReceiptDetails.h"
#import "BssReceiptAddress.h"
#import "BssCurrencyInfoModel.h"

@interface BssDatabase : NSObject

/*!
 * @discussion Use this instance for all method calls inside this class.
 */
+ (instancetype _Nonnull )sharedInstance;

#pragma mark - Reset
/*!
 * @discussion Total reset of the database. Deletes all entites stored in core data.
 */
+ (void)reset;


#pragma mark - Bundle Settings
@property (nullable, nonatomic) NSString *pushToken;
@property (nullable, nonatomic, readonly) NSString *initialInstallVersion;
@property (nullable, nonatomic, readonly) NSString *mobileClientId;
@property (nullable, nonatomic, readonly) NSString *loginType;
@property (nullable, nonatomic, readonly) NSString *isoCountryCode;


#pragma mark - Keychain (Read-only)
@property (nullable, nonatomic, readonly) NSString *accessToken;
@property (nullable, nonatomic, readonly) NSString *refreshToken;
@property (nullable, nonatomic, readonly) NSString *userName;

- (NSString * _Nullable)accessTokenForMobileClientWithId:(NSString * _Nonnull)mobileClientId;
- (NSString * _Nullable)accessTokenForMobileClientWithId:(NSString * _Nonnull)mobileClientId error:(NSError * _Nullable __autoreleasing * _Nullable)error;
- (NSString * _Nullable)refreshTokenForMobileClientWithId:(NSString * _Nonnull)mobileClientId;
- (NSString * _Nullable)refreshTokenForMobileClientWithId:(NSString * _Nonnull)mobileClientId error:(NSError * _Nullable __autoreleasing * _Nullable)error;
- (NSString * _Nullable)userNameForMobileClientWithId:(NSString * _Nonnull)mobileClientId;
- (NSString * _Nullable)userNameForMobileClientWithId:(NSString * _Nonnull)mobileClientId error:(NSError * _Nullable __autoreleasing * _Nullable)error;

- (void)setDidForceKeychainAccessibilityAlways:(BOOL)didForceKeychainAccessibilityAlways;
- (BOOL)didForceKeychainAccessibilityAlways;

#pragma mark - BssCatalogElements
/*!
 * @discussion Currently number stored catalog elements in core data.
 * @return Number of catalog elements which are currently stored in core data.
 */
- (int)numberOfCatalogElements;

/*!
 * @discussion Currently number of stored catalogElements in core data, depending on the predicate.
 * @param predicate Custom predicate to filter the returned result.
 * @return Number of catalog elements which are currently stored in core data and which are suitable to the given predicate.
 */
- (int)numberOfCatalogElementsWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Currently stored catalog elements in core data.
 * @return Alphabetically sorted catalog elements which are currently stored in core data (sorted by displayedName).
 */
- (NSArray<BssCatalogElement *> * _Nonnull)catalogElements;

/*!
 * @discussion Currently stored catalog elements in core data, depending on the predicate.
 * @param predicate Custom predicate to filter the returned result.
 * @return Alphabetically sorted catalog elements which are currently stored in core data and which are suitable to the given predicate  (sorted by displayedName).
 */
- (NSArray<BssCatalogElement *> * _Nonnull)catalogElementsWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Delete all currently stored catalot elements. Be careful to delete only catalog elements that have already been deleted on server or never synchronzed with the server.
 */
- (void)deleteAllCatalogElements;

#pragma mark BssCard
/*!
 * @discussion Currently stored card elements in core data, depending on the predicate.
 * @param predicate Custom predicate to filter the returned result.
 * @return Several card elements which are currently stored in core data and which are suitable to the given predicate.
 */
- (NSArray<BssCard *> * _Nonnull)cardsWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Stored card in core data which suits to the given id.
 * @param cardId Id that matches the uniqueId of the stored card.
 * @return Stored card in core data which suits to the given id.
 */
- (BssCard * _Nullable)cardWithId:(NSString * _Nonnull)cardId;

/*!
 * @discussion Stored card in core data which suits to the given predicate.
 * @param predicate Predicate that matches with the properties of the stored card.
 * @return Stored card in core data which suits to the given predicate.
 */
- (BssCard * _Nullable)cardWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Save given card in core data. This is useful especially for ignored properties (non server properties). The card will not be synced with the server.
 * @param card The card which will be updated in core data.
 */
- (void)saveCard:(BssCard * _Nonnull)card;

/*!
 * @discussion Delete a card in core data. Be careful to delete only cards that have already been deleted on server or never synchronzed with the server.
 * @param card The card that will be deleted in core data.
 */
- (void)deleteCard:(BssCard * _Nonnull)card;

/*!
 * @discussion Delete a card with a given id in core data. Be careful to delete only cards that have already been deleted on server or never synchronzed with the server.
 * @param cardId The unique id of a locally stored card. If card does not exist, nothing happens.
 * @return YES if the card has been deleted, NO if the card was not found in core data.
 */
- (BOOL)deleteCardWithId:(NSString * _Nonnull)cardId;

#pragma mark BssInsurance
/*!
 * @discussion Stored insurance in core data which suits to the given id.
 * @param insuranceId Id that matches the uniqueId of the stored insurance.
 * @return Stored insurance in core data which suits to the given id.
 */
- (BssInsurance * _Nullable)insuranceWithId:(NSString * _Nonnull)insuranceId;

/*!
 * @discussion Stored insurance in core data which suits to the given predicate.
 * @param predicate Predicate that matches with the properties of the stored insurance.
 * @return Stored insurance in core data which suits to the given predicate.
 */
- (BssInsurance * _Nullable)insuranceWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Save given insurance in core data. This is useful especially for ignored properties (non server properties). The insurance will not be synced with the server.
 * @param insurance The insurance which will be updated in core data.
 */
- (void)saveInsurance:(BssInsurance * _Nonnull)insurance;

/*!
 * @discussion Delete a insurance in core data. Be careful to delete only insurance that have already been deleted on server or never synchronzed with the server.
 * @param insurance The insurance that will be deleted in core data.
 */
- (void)deleteInsurance:(BssInsurance * _Nonnull)insurance;

/*!
 * @discussion Delete a insurance with a given id in core data. Be careful to delete only insurance that have already been deleted on server or never synchronzed with the server.
 * @param insuranceId The unique id of a locally stored insurnace. If insurance does not exist, nothing happens.
 * @return YES if the insurance has been deleted, NO if the insurance was not found in core data.
 */
- (BOOL)deleteInsuranceWithId:(NSString * _Nonnull)insuranceId;


#pragma mark BssCardType
/*!
 * @discussion Stored cardtype in core data which suits to the given id.
 * @param cardTypeId Id that matches the uniqueId of the stored cardtype.
 * @return Stored cardtype in core data which suits to the given id.
 */
- (BssCardType * _Nullable)cardTypeWithId:(NSString * _Nonnull)cardTypeId;

/*!
 * @discussion Stored cardtype in core data which suits to the given predicate.
 * @param predicate Predicate that matches with the properties of the stored cardtype.
 * @return Stored cardtype in core data which suits to the given predicate.
 */
- (BssCardType * _Nullable)cardTypeWithPredicate:(NSPredicate * _Nullable)predicate;


#pragma mark - BssItem
/*!
 * @discussion Number of stored items in core data.
 * @return Number of items which are currently stored in core data.
 */
- (int)numberOfItems;

/*!
 * @discussion Number of stored items in core data, depending on the predicate.
 * @param predicate Custom predicate to filter the returned result.
 * @return Number of items which are currently stored in core data and which are suitable to the given predicate.
 */
- (int)numberOfItemsWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Stored items in core data.
 * @return Items which are currently stored in core data.
 */
- (NSArray<BssItem *> * _Nonnull)items;

/*!
 * @discussion Stored itmes in core data, depending on the predicate.
 * @param predicate Custom predicate to filter the returned result.
 * @return Items which are currently stored in core data and which are suitable to the given predicate.
 */
- (NSArray<BssItem *> * _Nonnull)itemsWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Stored item in core data which suits to the given id.
 * @param itemId Id that matches the uniqueId of the stored item.
 * @return Stored item in core data which suits to the given id.
 */
- (BssItem * _Nullable)itemWithId:(NSString * _Nonnull)itemId;

/*!
 * @discussion Stored item in core data which suits to the given predicate.
 * @param predicate Predicate that matches with the properties of the stored item.
 * @return Stored item in core data which suits to the given predicate.
 */
- (BssItem * _Nullable)itemWithPredicate:(NSPredicate * _Nonnull)predicate;

/*!
 * @discussion Delete a specific item in core data.
 * @param item The item that will be deleted in core data.
 */
- (void)deleteItem:(BssItem * _Nonnull)item;

/*!
 * @discussion Delete an item with a given id
 * @param itemId The unique id of the item to be deleted
 */
- (void)deleteItemWithId:(NSString * _Nonnull)itemId;

/*!
 * @discussion Delete all currently stored items.
 */
- (void)deleteAllItems;

/*!
 * @discussion Delete items that suits to the given predicate.
 * @param predicate Custom predicate to delete only a group of items that suits to the predicate.
 */
- (void)deleteItemsWithPredicate:(NSPredicate * _Nullable)predicate;


#pragma mark - BssClientConfiguration
/*!
 * @discussion Stored configurations in core data.
 * @return Configurations which are currently stored in core data.
 */
- (NSArray<BssClientConfiguration *> * _Nonnull)clientConfigurations;

/*!
 * @discussion Stored configuration in core data which suit to the given predicate.
 * @param predicate Predicate that matches with the properties of the stored configuration.
 * @return Stored configurations in core data which suits to the given predicate.
 */
- (NSArray<BssClientConfiguration *> * _Nonnull)clientConfigurationsWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Stored configuration with the given key, or nil.
 * @param key The given key for the configuration. Possible key are: offers_section_enabled, bluecode_enabled, geofence_enabled.
 * @return Stored configuration in core data that suits to the given key. Nil in case of an incorrect key.
 */
- (BssClientConfiguration * _Nullable)clientConfigurationWithKey:(NSString * _Nonnull)key;

/*!
* @discussion Reads the value of the given key as a bool representation. If the key is not found in db, false will be returned..
* @param key The given key for the configuration. Possible key are:  offers_section_enabled, bluecode_enabled, geofence_enabled.
* @return Boolean representation of the value for the given key.
*/
- (BOOL)clientConfigurationValueForKey:(NSString * _Nonnull)key;

/*!
 * @discussion Delete all currently stored configurations.
 */
- (void)deleteAllClientConfigurations;

/*!
 * @discussion Delete configurations that suits to the given predicate.
 * @param predicate Custom predicate to delete only a group of configurations that suits to the predicate.
 */
- (void)deleteClientConfigurationsWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Delete a configuration with a certain key.
 * @param key The key for the configuration.
 */
- (void)deleteClientConfigurationWithKey:(NSString * _Nonnull)key;


#pragma mark - BssClientSettings
/*!
 * @discussion Stored settings in core data.
 * @return Settings which are currently stored in core data.
 */
- (NSArray<BssClientSettings *> * _Nonnull)clientSettings;

/*!
 * @discussion Stored item in core data which suit to the given predicate.
 * @param predicate Predicate that matches with the properties of the stored settings.
 * @return Stored settings in core data which suits to the given predicate.
 */
- (NSArray<BssClientSettings *> * _Nonnull)clientSettingsWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Stored setting in core data which suits to the given id. If the setting is not set yet, a new one will be created and returned with the given key.
 * The setting will not be synced with the server.
 * @param key Unique key for the setting.
 * @return Setting with the given key.
 */
- (BssClientSettings * _Nonnull)clientSettingWithKey:(NSString * _Nonnull)key;

/*!
 * @discussion Delete all currently stored settings.
 */
- (void)deleteAllClientSettings;

/*!
 * @discussion Delete settings that suits to the given predicate.
 * @param predicate Custom predicate to delete only a group of settings that suits to the predicate.
 */
- (void)deleteClientSettingsWithPredicate:(NSPredicate * _Nullable)predicate;

/*!
 * @discussion Delete a setting with a certain key.
 * @param key The defined key for the setting.
 */
- (void)deleteClientSettingWithKey:(NSString * _Nonnull)key;

#pragma mark - BssReceipt/BssReceiptProduct/BssReceiptAddress
/*!
 * @discussion Save a receipt, if a receipt already saved with the samed receipt id, it will be overriden.
 * @param receipt The receipt to save.
 */
- (void)saveReceiptDetails:(BssReceiptDetails* _Nonnull)receipt;

/*!
 * @discussion Save a list of receipts, if a receipt in the list is already saved with the samed receipt id, it will be overriden.
 * @param receipts A list of receipts to save.
 */
- (void)saveReceiptDetailsList:(NSMutableArray<BssReceiptDetails*>* _Nonnull)receipts;

/*!
 * @discussion Delete a receipt, if this receipt is the last one, which has a reference to the creation address, it also will be deleted.
 * @param receipt The receipt to delete.
 */
- (void)deleteReceiptDetails:(BssReceiptDetails* _Nonnull)receipt;

/*!
 * @discussion Deletes both all receipts from a cardId and all relating products (related entity)
 */
- (void)deleteAllReceiptDetailsFrom:(NSString * _Nonnull) cardId;

/*!
 * @discussion Delete invalid receipts that have no cardid but are still stored in core data.
 */
- (void)deleteInvalidReceipts;

/*!
 * @discussion Return all in persisted receipts of a carduser
 * @return NSMutableArray of BssReceipt.
 */
- (NSMutableArray<BssReceiptDetails*>* _Nullable)allReceiptDetailsFrom:(NSString * _Nonnull) cardId;

/*!
 * @discussion Return the first found in core data receiptDetails object with the given receiptId
 * @param receiptId The receiptId of the receiptDetails object in core data
 * @return a BssReceiptDetails object wich matches the receiptId
 */
- (BssReceiptDetails* _Nullable)getReceiptDetailsWithReceiptId:(NSString* _Nonnull)receiptId;


#pragma mark - Terms and Conditions
/*!
 * @discussion Check before initializing the BssBaseKit, if the Terms & Conditions are already accepted for the given versions.
 * @param termsVersion  The current version of the terms.
 * @param privacyVersion The current version of the confitions.
 * @return True if the terms have already been accepted with the given version, otherwise false.
*/
- (BOOL)termsAcceptedWithTermsVersion:(NSString * _Nonnull)termsVersion privacyVersion:(NSString * _Nonnull)privacyVersion;

/*!
 * @discussion To be called if the terms and condiions are being accepted by the user.
 * @param termsVersion  The current version of the terms.
 * @param privacyVersion  The current version of the confitions.
*/
- (void)acceptTermsWithTermsVersion:(NSString * _Nonnull)termsVersion privacyVersion:(NSString * _Nonnull)privacyVersion;


#pragma mark - Currencies
- (void)updateCurrencyInfoModel:(BssCurrencyInfoModel * _Nullable)infoModel contributionKey:(BssCurrencyInfoModelContributionKey)contributionKey NS_SWIFT_NAME( updateCurrencyInfoModel(currencyInfoModel:contributionKey:) );
- (BssCurrencyInfoModel * _Nullable)currencyInfoModelForContributionKey:(BssCurrencyInfoModelContributionKey)contributionKey;

- (NSString *_Nonnull)defaultContributionKeyAsString;
@end
