//
//  BssCatalogElement.h
//  BssBaseKit
//
//  Created by Hager Florian on 12.07.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssImage.h"
#import "BssBase.h"
#import "BssCatalogElementImageDownloadInfo.h"
#import "BssCatalogElementTypeEnumClass.h"
#import "BssItemInfo.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BssItemInfo;

@interface BssCatalogElement : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * uniqueId;
@property (strong, nonatomic, nullable) NSString <Optional> * customName;
@property (strong, nonatomic, nullable) NSString <Optional> * notes;
@property (nonatomic, strong, nullable) BssImage <Optional> * customBackImage;
@property (strong, nonatomic, nonnull) NSString * type;
@property (nonatomic, strong, nonnull) NSArray<BssItemInfo *> <BssItemInfo> * itemInfoList;

+ (float)imageRatio;
- (BssCatalogElementTypeEnum)typeEnum;
- (void)loadImagesWithFrontImageInfo:(BssCatalogElementImageDownloadInfo * _Nullable)frontImageInfo backImageInfo:(BssCatalogElementImageDownloadInfo * _Nullable)backImageInfo completionBlock:(BssFailureBlock _Nullable)completed;

- (NSComparisonResult)compareWithOtherCatalogElement:(BssCatalogElement * _Nonnull)otherCatalogElement byAttribute:(NSString * _Nonnull)attribute;

// overwritten by child classes
@property (nonatomic, strong, nullable, readonly) NSString <Ignore> * frontImageUrl;
@property (nonatomic, strong, nullable, readonly) NSString <Ignore> * backImageUrl;
- (BOOL)hasFrontImage;
- (BOOL)hasCustomFrontImage;
- (BOOL)hasBackImage;
- (BOOL)hasCustomBackImage;
- (BOOL)isStoredFrontImageAvailable;
- (BOOL)isStoredBackImageAvailable;
- (nonnull NSString *)displayedName;
- (nullable UIImage *)displayedCardImage;

#pragma mark - Non-Synced Properties
@property (nonatomic, assign) int32_t orderIndex;

#pragma mark - Database features
/// save and update card in core data. If no entity is created yet, it will be. All properties (also the ignored ones) will be overwritten. Attention: All synced properties will be overwritten after the next server sync again.
- (void)save;
/// update a specific card property in coredata. If the card is not found in database, NO will be returned. If the propery cannot be mapped, an exception will be thrown.
/// It is recommended to use it via NSStringFromSelector like [card savePropertyWithName:NSStringFromSelector(@selector(customName))];
- (void)savePropertyWithName:(nonnull NSString *)propertyName;
- (void)deleteCatalogElement;

@end

NS_ASSUME_NONNULL_END
