//
//  BssBalance.h
//  BssBaseKit
//
//  Created by Hager Florian on 11.08.20.
//  Copyright © 2020 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import "BssJSONModel.h"
#import "BssMonetaryAmount.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssExtensionBalance : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * reference;
@property (strong, nonatomic, nullable) BssMonetaryAmount <Optional> * amount;

@end

NS_ASSUME_NONNULL_END
