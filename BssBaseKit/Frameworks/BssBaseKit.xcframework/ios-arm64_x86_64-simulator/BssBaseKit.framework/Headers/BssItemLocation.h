//
//  BssItemLocation.h
//  BssBaseKit
//
//  Created by Florian Hager on 21.12.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BssJSONModel.h"

@interface BssItemLocation : BssJSONModel

@property (nonatomic, assign) CGFloat latitude;
@property (nonatomic, assign) CGFloat longitude;

@property (strong, nonatomic, nullable) NSString <Optional> * name;
@property (strong, nonatomic, nullable) NSString <Optional> * additionalName;
@property (strong, nonatomic, nullable) NSString <Optional> * street;
@property (strong, nonatomic, nullable) NSString <Optional> * zip;
@property (strong, nonatomic, nullable) NSString <Optional> * city;
@property (strong, nonatomic, nullable) NSString <Optional> * state;
@property (strong, nonatomic, nullable) NSString <Optional> * country;
@property (strong, nonatomic, nullable) NSString <Optional> * email;
@property (strong, nonatomic, nullable) NSString <Optional> * webUri;
@property (strong, nonatomic, nullable) NSString <Optional> * phoneNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * faxNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * facebookUri;
@property (strong, nonatomic, nullable) NSString <Optional> * twitterUri;

@end
