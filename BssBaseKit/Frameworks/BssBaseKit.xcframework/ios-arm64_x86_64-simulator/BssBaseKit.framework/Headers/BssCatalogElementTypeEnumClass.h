//
//  BssCatalogElementTypeEnumClass.h
//  BssBaseKit
//
//  Created by Hager Florian on 18.07.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    BssCatalogElementTypeCard,
    BssCatalogElementTypeInsurance,
    BssCatalogElementTypeUnknown
} BssCatalogElementTypeEnum;

@interface BssCatalogElementTypeEnumClass : NSObject

+ (BssCatalogElementTypeEnum)convertToEnum:(NSString * _Nullable)type;

@end

NS_ASSUME_NONNULL_END
