//
//  BssCardUserDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssCardUser : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * activityType;
// sign-up
@property (strong, nonatomic, nullable) NSString <Optional> * title;
@property (strong, nonatomic, nullable) NSString <Optional> * firstName;
@property (strong, nonatomic, nullable) NSString <Optional> * lastName;
@property (strong, nonatomic, nullable) NSString <Optional> * company;
@property (strong, nonatomic, nullable) NSString <Optional> * street1;
@property (strong, nonatomic, nullable) NSString <Optional> * houseNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * street2;
@property (strong, nonatomic, nullable) NSString <Optional> * zip;
@property (strong, nonatomic, nullable) NSString <Optional> * city;
@property (strong, nonatomic, nullable) NSString <Optional> * countryIsoCode;
@property (strong, nonatomic, nullable) NSString <Optional> * email;
@property (strong, nonatomic, nullable) NSString <Optional> * phoneNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * mobilePhoneNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * fax;
@property (strong, nonatomic, nullable) NSString <Optional> * birthday;
@property (strong, nonatomic, nullable) NSString <Optional> * gender;
@property (strong, nonatomic, nullable) NSString <Optional> * otherGenderDetails;

- (nullable NSString *)country;
- (BOOL)isGenderTypeMale;
- (BOOL)isGenderTypeFemale;
- (BOOL)isGenderTypeOther;

@end
