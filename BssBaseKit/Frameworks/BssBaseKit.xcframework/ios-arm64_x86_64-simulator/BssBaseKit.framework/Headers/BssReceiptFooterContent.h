//
//  BssReceiptFooterContent.h
//  BssPSALoyalty
//
//  Created by Viacheslav Korkhonen on 08.03.19.
//  Copyright © 2019 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssReceiptFooterContent : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * column1;
@property (strong, nonatomic, nullable) NSString <Optional> * column2;
@property (strong, nonatomic, nullable) NSString <Optional> * column3;

@end

NS_ASSUME_NONNULL_END
