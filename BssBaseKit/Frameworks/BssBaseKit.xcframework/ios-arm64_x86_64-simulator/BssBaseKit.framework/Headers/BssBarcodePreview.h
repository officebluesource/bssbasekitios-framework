//
//  BssBarcodePreviewDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssBarcodePreview : BssJSONModel

@property (strong, nonatomic) NSString * type;
@property (strong, nonatomic) NSString * uri;

@end
