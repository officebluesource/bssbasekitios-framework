//
//  BssContentElementDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

typedef enum {
    BssContentElementTypeUnknown,
    BssContentElementTypeInternalUrl,
    BssContentElementTypeExternalUrl,
    BssContentElementTypelVideo,
    BssContentElementTypeHtmlText,
    BssContentElementTypePhone,
    BssContentElementTypeEmail,
    BssContentElementTypeBarcode
} BssContentElementType;

@interface BssContentElement : BssJSONModel

@property (nonatomic, strong, nonnull) NSString * uniqueId;
@property (nonatomic, strong, nonnull) NSString * name;
@property (nonatomic, strong, nonnull) NSString * type;

// INTERNAL_LINK
@property (nonatomic, strong, nullable) NSString <Optional> * internalUrl;

// EXTERNAL_LINK
@property (nonatomic, strong, nullable) NSString <Optional> * externalUrl;

// VIDEO
@property (nonatomic, strong, nullable) NSString <Optional> * youtubeUrl;

// HTML_TEXT
@property (nonatomic, strong, nullable) NSString <Optional> * htmlText;
@property (nonatomic, assign) BOOL htmlTextPreviewEnabled;

// PHONE
@property (nonatomic, strong, nullable) NSString <Optional> * phoneNumber;

// EMAIL
@property (nonatomic, strong, nullable) NSString <Optional> * email;

// BARCODE
@property (nonatomic, strong, nullable) NSString <Optional> * barcodeNumber;
@property (nonatomic, strong, nullable) NSString <Optional> * barcodeType;
@property (nonatomic, strong, nullable) NSString <Optional> * barcodeUri;

- (BssContentElementType)typeEnum;

@end
