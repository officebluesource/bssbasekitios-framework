//
//  BssChaChingTransaction.h
//  BssBaseKit
//
//  Created by Hager Florian on 10.07.20.
//  Copyright © 2020 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssMonetaryAmount.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssChaChingTransaction : BssJSONModel

typedef enum {
    BssReversalStatusNone,
    BssReversalStatusReversible,
    BssReversalStatusReversed,
    BssReversalStatusReverses
} BssReversalStatus;

@property (strong, nonatomic, nonnull) NSString * reference;
@property (strong, nonatomic, nonnull) NSString * descr;
@property (strong, nonatomic, nonnull) NSDate * createdDate;
@property (strong, nonatomic, nonnull) NSString * reversalStatus;
@property (strong, nonatomic, nullable) BssMonetaryAmount <Optional> * amount;

- (BssReversalStatus)reversalStatusEnum;

@end

NS_ASSUME_NONNULL_END
