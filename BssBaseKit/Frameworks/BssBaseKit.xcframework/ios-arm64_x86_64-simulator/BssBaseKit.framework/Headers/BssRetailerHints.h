//
//  BssRetailerHints.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssImage.h"
#import "BssColorScheme.h"
#import "BssBase.h"
#import "BssExternalId.h"

@protocol BssExternalId;

@interface BssRetailerHints : BssJSONModel

@property (nonatomic, assign) BOOL hasContact;
@property (nonatomic, assign) BOOL hasStores;
@property (nonatomic, assign) BOOL checkoutReceiptScanEnabled;

@property (strong, nonatomic, nonnull) NSString * retailerId;
@property (strong, nonatomic, nonnull) NSString * name;
@property (strong, nonatomic, nullable) NSString <Optional> * retailerCity;

@property (strong, nonatomic, nullable) BssImage <Optional> * logo;
@property (strong, nonatomic, nullable) BssColorScheme <Optional> * retailerColorScheme;

@property (nonatomic, strong, nonnull) NSArray<BssExternalId *> <BssExternalId> * externalIds;

- (void)saveLogoImageWithSuccess:(nullable BssEmptyBlock)success failure:(nullable BssFailureBlock)failure;
- (void)savePropertyWithName:(nonnull NSString *)propertyName;
@end
