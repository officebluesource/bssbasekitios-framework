//
//  BssHoferBankCardAutoSubscribe.h
//  BssBaseKit
//
//  Created by Korkhonen Viacheslav on 23.05.19.
//  Copyright © 2019 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BssJSONModel.h"

@interface BssHoferBankCardAutoSubscribe : BssJSONModel

@property (nonatomic, assign) BOOL autoSubscription;

@end

