//
//  BssExtensionStatus.h
//  BssBaseKit
//
//  Created by Hager Florian on 15.09.20.
//  Copyright © 2020 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssExtensionStatus : BssJSONModel

@property (nonatomic) int32_t code;
@property (strong, nonatomic, nonnull) NSString * message;

@end

NS_ASSUME_NONNULL_END
