//
//  BssIssuerDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssContact.h"

@interface BssIssuer : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * name;
@property (strong, nonatomic, nullable) BssContact <Optional> * address;

@end
