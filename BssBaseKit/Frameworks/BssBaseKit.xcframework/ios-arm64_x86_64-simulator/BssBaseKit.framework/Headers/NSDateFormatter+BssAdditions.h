//
//  NSDateFormatter+BssAdditions.h
//  BssBaseKit
//
//  Created by Florian Hager on 18.09.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (BssAdditions)

+ (NSDateFormatter * _Nonnull)BSS_HoferDateFormatter;
+ (NSDateFormatter * _Nonnull)BSS_dateFormatter;
+ (NSDateFormatter * _Nonnull)BSS_dateFormatterSimple;
+ (NSDateFormatter * _Nonnull)BSS_dateFormatterDayAndMonth;
+ (NSDateFormatter * _Nonnull)BSS_relativeDateFormatter;

@end
