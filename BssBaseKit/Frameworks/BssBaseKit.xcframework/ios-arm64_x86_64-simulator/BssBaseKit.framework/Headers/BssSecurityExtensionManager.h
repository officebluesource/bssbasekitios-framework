//
//  BssSecurityExtensionManager.h
//  BssBaseKit
//
//  Created by Florian Hager on 26.12.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssBase.h"
#import "BssCard.h"
#import "BssCardType.h"

typedef void (^BssUpdateDynmicBarcodesFailureBlock)(NSArray<NSError *> * _Nullable errors, NSArray<BssCard *> * _Nullable erroredCards);

@interface BssSecurityExtensionManager : NSObject

+ (BssSecurityExtensionManager * _Nonnull)sharedInstance;

// card activation
- (BOOL)cardActivated:(BssCard * _Nonnull)card;
- (void)activateCard:(BssCard * _Nonnull)card withUsername:(NSString* _Nonnull)username withPin:(NSString * _Nonnull)pin success:(BssEmptyBlock _Nullable)success failure:(BssFailureBlock _Nullable)failure;

@end
