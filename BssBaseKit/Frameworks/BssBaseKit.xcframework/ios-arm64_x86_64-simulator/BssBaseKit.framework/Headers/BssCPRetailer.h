//
//  BssCPRetailer.h
//  BssBaseKit
//
//  Created by Baumgartner Mario on 28.06.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssContact.h"
#import "BssImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssCPRetailer : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * publicId;
@property (strong, nonatomic, nonnull) NSString * name;

@property (strong, nonatomic, nonnull) BssContact * contact;
@property (strong, nonatomic, nullable) BssImage <Optional> * logo;

@end

NS_ASSUME_NONNULL_END
