//
//  BssTag.h
//  BssBaseKit
//
//  Created by Florian Hager on 23.10.18.
//  Copyright © 2018 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssTag : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * descr;

@end
