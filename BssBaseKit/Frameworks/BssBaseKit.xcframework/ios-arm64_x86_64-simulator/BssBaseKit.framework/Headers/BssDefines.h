//
//  BssDefines.h
//  BssBaseKit
//
//  Created by Florian Hager on 09.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#ifndef BssDefines_h
#define BssDefines_h

/* Screen */
#define BSS_SCREENHEIGHT_3_5INCH 480.0
#define BSS_SCREENHEIGHT_4INCH 568.0
#define BSS_SCREENHEIGHT_4_7INCH 667.0
#define BSS_SCREENHEIGHT_5_5INCH 736.0
#define BSS_SCREENWIDTH ([[UIScreen mainScreen] bounds].size.width)
#define BSS_SCREENHEIGHT ([[UIScreen mainScreen] bounds].size.height)

/* System Version */
#define BSS_IOS10_AND_HIGHER ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0)
#define BSS_IOS9_AND_HIGHER ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0)
#define BSS_IOS8_AND_HIGHER ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)

/* Urls */
#define BSS_BASE_URL_PROD @"https://rest-b2c.mobile-pocket.com"
#define BSS_BASE_URL_PREPROD @"https://rest-b2c-preprod.mp-testing.com"
#define BSS_BASE_URL_STAGING @"https://rest-b2c-staging.mp-testing.com"
#define BSS_BASE_URL_DEV @"https://rest-b2c-dev.mp-testing.com"
#define BSS_BASE_URL_BARNEY_PROD_0 @"http://mp-barney-prod0.rest-b2c.mobile-pocket.com"
#define BSS_BASE_URL_BARNEY_PROD_1 @"http://mp-barney-prod1.rest-b2c.mobile-pocket.com"
#define BSS_BASE_URL_MOE_PROD_0 @"http://mp-moe-prod0.rest-b2c.mobile-pocket.com"
#define BSS_BASE_URL_MOE_PROD_1 @"http://mp-moe-prod1.rest-b2c.mobile-pocket.com"
#define BSS_BASE_URL_SELMA_STAGING_0 @"http://mp-staging0.rest-b2c-staging.mp-testing.com"
#define BSS_BASE_URL_SELMA_STAGING_1 @"http://mp-staging1.rest-b2c-staging.mp-testing.com"
#define BSS_BASE_URL_SELMA_PREPROD_0 @"http://mp-preprod0.rest-b2c-preprod.mp-testing.com"
#define BSS_BASE_URL_MILHOUSE_DEV_0 @"http://mp-dev0.rest-b2c-dev.mp-testing.com"
#define BSS_BASE_URL_MILHOUSE_DEV_1 @"http://mp-dev1.rest-b2c-dev.mp-testing.com"
#define BSS_BASE_URL_CUSTOM @"http://10.24.100.0:8888"

/* Authentication */
// !! outdated - will not be extended any more with future login types !!
#define BSS_LOGIN_TYPE_ANONYMOUS @"MP_ANONYMOUS"
#define BSS_LOGIN_TYPE_USERNAME_PASSWORD @"MP_USERNAME_PASSWORD"
#define BSS_LOGIN_TYPE_FACEBOOK @"FACEBOOK"
#define BSS_LOGIN_TYPE_GOOGLE @"GOOGLE"
#define BSS_LOGIN_TYPE_VODAFONE_WALLET @"VODAFONE_WALLET"
#define BSS_LOGIN_TYPE_VODAFONE_MVA @"VODAFONE_MVA"
#define BSS_LOGIN_TYPE_BBVA @"BBVA"
#define BSS_LOGIN_TYPE_INTESA @"INTESA"
#define BSS_LOGIN_TYPE_PSA @"PSA"
#define BSS_LOGIN_TYPE_JWT @"JWT"
#define BSS_LOGIN_TYPE_APPLE @"APPLE"
#define BSS_LOGIN_TYPE_SLOYALTY @"S_LOYALTY"
#define BSS_LOGIN_TYPE_CP_GOODIO @"CP_GOODIO"
#define BSS_LOGIN_TYPE_CP_GOODIO_ANONYMOUS @"CP_GOODIO_ANONYMOUS"
#define BSS_LOGIN_TYPE_CP_GEMMA_M4 @"CP_GEMMA_M4"
#define BSS_LOGIN_TYPE_CP_GEMMA_M4_ANONYMOUS @"CP_GEMMA_M4_ANONYMOUS"

/* App Type */
#define BSS_APP_TYPE_MOBILE_POCKET @"MOBILE_POCKET"
#define BSS_APP_TYPE_SHOW_CARD @"SHOW_CARD"
#define BSS_APP_TYPE_DEMO @"DEMO"
#define BSS_APP_TYPE_BBVA @"BBVA"
#define BSS_APP_TYPE_INTESA @"INTESA"
#define BSS_APP_TYPE_MVA @"MVA"
#define BSS_APP_TYPE_SLOYALTY @"S_LOYALTY"
#define BSS_APP_TYPE_CP_GOODIO @"CP_GOODIO"
#define BSS_APP_TYPE_CP_GEMMA_M4 @"CP_GEMMA_M4"
#define BSS_APP_TYPE_HUAWEI @"HUAWEI"

/* Barcode Types */
#define BSS_BARCODE_TYPE_UNKOWN @"UNKNOWN"
#define BSS_BARCODE_TYPE_AZTEC @"AZTEC_CODE" // not scanable right now
#define BSS_BARCODE_TYPE_CODABAR @"CODABAR"
#define BSS_BARCODE_TYPE_CODE39 @"CODE_39"
#define BSS_BARCODE_TYPE_CODE93 @"CODE_93"
#define BSS_BARCODE_TYPE_CODE128 @"CODE_128"
#define BSS_BARCODE_TYPE_CODE128A @"CODE_128_A" // for barcodewhisperer needed
#define BSS_BARCODE_TYPE_CODE128B @"CODE_128_B" // for barcodewhisperer needed
#define BSS_BARCODE_TYPE_CODE128C @"CODE_128_C" // for barcodewhisperer needed
#define BSS_BARCODE_TYPE_DATAMATRIX @"DATA_MATRIX" // not scanable right now
#define BSS_BARCODE_TYPE_EAN8 @"EAN_8"
#define BSS_BARCODE_TYPE_EAN13 @"EAN_13"
#define BSS_BARCODE_TYPE_EAN128 @"EAN_128" // not scanable right now
#define BSS_BARCODE_TYPE_I25 @"INTERLEAVED_2_OF_5"
#define BSS_BARCODE_TYPE_PDF417 @"PDF417"
#define BSS_BARCODE_TYPE_S25 @"STANDARD_2_OF_5"
#define BSS_BARCODE_TYPE_UPCA @"UPC_A"
#define BSS_BARCODE_TYPE_QRCODE @"QR_CODE"
#define BSS_BARCODE_TYPE_GS1_DATABAR @"GS1_DATABAR" // not scanable right now
#define BSS_BARCODE_TYPE_GS1_DATABAR_STACKED @"GS1_DATABAR_STACKED" // not scanable right now
#define BSS_BARCODE_TYPE_GS1_DATABAR_LIMITED @"GS1_DATABAR_LIMITED" // not scanable right now
#define BSS_BARCODE_TYPE_GS1_DATABAR_EXPANDED @"GS1_DATABAR_EXPANDED" // not scanable right now

/* Configurations */
#define BSS_CLIENT_CONFIGURATION_KEY_OFFERS_SECTION_ENABLED @"offers_section_enabled"
#define BSS_CLIENT_CONFIGURATION_KEY_BLUECODE_ENABLED @"bluecode_enabled"
#define BSS_CLIENT_CONFIGURATION_KEY_GEOFENCE_ENABLED @"geofence_enabled"

/* Logging */
#define BSS_CURRENT_FUNCTION [NSString stringWithFormat:@"%s(%d)", __PRETTY_FUNCTION__, __LINE__]

#endif /* BssDefines_h */
