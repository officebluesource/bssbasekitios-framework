//
//  BssColoredButton.h
//  BssBaseKit
//
//  Created by Florian Hager on 18.09.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BssColoredButton : UIButton

- (void)setColorForButton:(UIColor * _Nonnull)color;
- (void)setImageForButton:(UIImage * _Nonnull)image;
- (void)setBorderForButtonWithColor:(UIColor * _Nonnull)color borderWidth:(CGFloat)borderWidth;
- (void)setTitleColorForButton:(UIColor * _Nonnull)titleColor;
- (void)setFontForButton:(UIFont * _Nonnull)font;

@end
