//
//  BssCountryDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

@interface BssCountry : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * isoCode;
@property (strong, nonatomic, nonnull) NSString * name;
@property (strong, nonatomic, nonnull) NSString * uniqueId;
@property (nonatomic, assign) BOOL selected;

@end
