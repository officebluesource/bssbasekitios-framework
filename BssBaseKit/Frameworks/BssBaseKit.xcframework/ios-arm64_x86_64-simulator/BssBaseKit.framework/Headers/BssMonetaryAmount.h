//
//  BssMonetaryAmount.h
//  BssBaseKit
//
//  Created by Hager Florian on 10.07.20.
//  Copyright © 2020 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssMonetaryAmount : BssJSONModel

@property (strong, nonatomic, nullable) NSNumber <Optional> * number;
@property (strong, nonatomic, nullable) NSString <Optional> * currency;

@end

NS_ASSUME_NONNULL_END
