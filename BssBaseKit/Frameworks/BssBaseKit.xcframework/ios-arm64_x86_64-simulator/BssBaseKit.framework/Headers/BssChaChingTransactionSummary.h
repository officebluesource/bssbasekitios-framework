//
//  BssChaChingTransactionSummary.h
//  BssBaseKit
//
//  Created by Manya Egon on 28.06.22.
//  Copyright © 2022 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssExtensionBalance.h"
#import "BssChaChingTransaction.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssChaChingTransactionSummary : BssJSONModel

@property (strong, nonatomic, nonnull) BssChaChingTransaction * transaction;
@property (strong, nonatomic, nonnull) BssExtensionBalance * balance;

@end

NS_ASSUME_NONNULL_END
