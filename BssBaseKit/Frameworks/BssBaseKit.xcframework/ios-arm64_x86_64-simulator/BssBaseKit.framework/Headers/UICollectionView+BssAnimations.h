//
//  UICollectionView+BssAnimations.h
//  BssBaseKit
//
//  Created by Florian Hager on 15.09.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BssBase.h"

typedef enum {
    BssUICollectionViewAnimationOptionFadeInNone,
    BssUICollectionViewAnimationOptionFadeOutNone,
    BssUICollectionViewAnimationOptionFadeOutVeryShort,
    BssUICollectionViewAnimationOptionFadeInVeryShort,
    BssUICollectionViewAnimationOptionFadeOutShort,
    BssUICollectionViewAnimationOptionFadeInShort,
    BssUICollectionViewAnimationOptionLinearShort,
    BssUICollectionViewAnimationOptionLinearLong
} BssUICollectionViewAnimationOption;

@interface UICollectionView (BssAnimations)

/**
 * Animation where the whole tableview fades first out, then in.
 * Default AnimationOption is UITableViewAnimationOptionLinearLong.
 */
- (void)BSS_reloadDataAnimated:(BOOL)animated;

/**
 * Animation where the whole tableview fades first out, then in.
 */
- (void)BSS_reloadDataAnimated:(BOOL)animated withAnimationOption:(BssUICollectionViewAnimationOption)animationOption completed:(BssEmptyBlock)completed;

/**
 * Animation where the whole tableview fades first out, then in.
 */
- (void)BSS_reloadDataAnimated:(BOOL)animated withFadeInDuration:(CGFloat)fadeInDuration fadeOutDuration:(CGFloat)fadeOutDuration completed:(BssEmptyBlock)completed;

/**
 * Animation where the whole tableview fades in
 */
- (void)BSS_fadeInAnimated:(BssUICollectionViewAnimationOption)animationOption completed:(BssEmptyBlock)completed;

/**
 * Animation where the whole tableview fades out
 */
- (void)BSS_fadeOutAnimated:(BssUICollectionViewAnimationOption)animationOption completed:(BssEmptyBlock)completed;

@end
