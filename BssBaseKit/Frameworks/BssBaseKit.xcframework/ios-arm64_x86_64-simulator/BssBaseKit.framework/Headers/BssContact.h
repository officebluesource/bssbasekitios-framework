//
//  BssContactDTO.h
//  BssBaseKit
//
//  Created by Florian Hager on 16.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssGeoLocation.h"

@interface BssContact : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * street;
@property (strong, nonatomic, nullable) NSString <Optional> * zip;
@property (strong, nonatomic, nullable) NSString <Optional> * country;
@property (strong, nonatomic, nullable) NSString <Optional> * city;
@property (strong, nonatomic, nullable) NSString <Optional> * phoneNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * faxNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * email;
@property (strong, nonatomic, nullable) NSString <Optional> * webUri;
@property (strong, nonatomic, nullable) NSString <Optional> * facebookUri;
@property (strong, nonatomic, nullable) NSString <Optional> * twitterUri;

@property (strong, nonatomic, nullable) BssGeoLocation <Optional> * geoLocation;

@end
