//
//  BssCatalogElementShareToken.h
//  BssBaseKit
//
//  Created by Hager Florian on 31.01.24.
//  Copyright © 2024 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssCatalogElementShareToken : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * shareToken;
@property (strong, nonatomic, nonnull) NSDate * validUntil;

@end

NS_ASSUME_NONNULL_END
