//
//  BssCatalogElementType.h
//  BssBaseKit
//
//  Created by Hager Florian on 06.07.23.
//  Copyright © 2023 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssImage.h"
#import "BssRetailerHints.h"
#import "BssCatalogElementTypeEnumClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssCatalogElementType : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * uniqueId;
@property (strong, nonatomic, nullable) BssImage <Optional> * frontImage;
@property (strong, nonatomic, nonnull) NSString * name;
@property (strong, nonatomic, nullable) BssRetailerHints <Optional> * retailerHints;
@property (strong, nonatomic, nullable) NSArray<NSString *> <Optional> * keywords;
@property (strong, nonatomic, nonnull) NSString * type;

- (BssCatalogElementTypeEnum)typeEnum;
- (BOOL)isRequestable;
- (BOOL)isCreatable;

@end

NS_ASSUME_NONNULL_END
