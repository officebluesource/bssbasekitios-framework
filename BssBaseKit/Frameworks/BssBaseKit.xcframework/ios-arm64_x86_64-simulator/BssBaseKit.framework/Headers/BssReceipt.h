#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

#import "BssReceiptAddress.h"
#import "BssReceiptFooterContent.h"
#import "BssReceiptProduct.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BssReceiptFooterContent, BssReceiptProduct;

@interface BssReceipt : BssJSONModel

@property (strong, nonatomic, nullable) NSString <Optional> * receiptId;
@property (strong, nonatomic, nullable) NSString <Optional> * receiptNumber;
@property (strong, nonatomic, nullable) NSString <Optional> * creationDate;
@property (strong, nonatomic, nullable) BssReceiptAddress <Optional> * creationAddress;
@property (strong, nonatomic, nullable) NSNumber <Optional> * totalPrice;
@property (strong, nonatomic, nullable) NSString <Optional> * currency;
@property (strong, nonatomic, nullable) NSString <Optional> * status;
@property (nonatomic, assign) BOOL favorite;

#pragma mark - Additions & Helper
-(nullable NSDate *) creationDateAsNSDate;
-(BOOL) isPending;

@end

NS_ASSUME_NONNULL_END
