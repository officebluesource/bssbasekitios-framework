//
//  BssLottery.h
//  BssBaseKit
//
//  Created by Hager Florian on 25.02.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssLotteryLink.h"
#import "BssOfflineLotteryOptions.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BssLotteryLink;

typedef enum {
    BssLotteryParticipationTypeUnknown,
    BssLotteryParticipationTypeRequired,
    BssLotteryParticipationTypeOptional,
    BssLotteryParticipationTypeHidden
} BssLotteryParticipationType;

@interface BssLottery : BssJSONModel

@property (strong, nonatomic, nonnull) NSString * participantEmailFieldType;
@property (strong, nonatomic, nonnull) NSString * participantPhoneNumberFieldType;
@property (strong, nonatomic, nonnull) BssLotteryLink * termsConditionsLink;
@property (strong, nonatomic, nullable) NSArray<BssLotteryLink *> <BssLotteryLink, Optional> * links;
@property (strong, nonatomic, nullable) BssOfflineLotteryOptions <Optional> * options;
@property (strong, nonatomic, nullable) NSNumber <Optional> * participationsPerUser;

- (BssLotteryParticipationType)participationTypeEnumForFieldType:(nonnull NSString *)type;

@end

NS_ASSUME_NONNULL_END
