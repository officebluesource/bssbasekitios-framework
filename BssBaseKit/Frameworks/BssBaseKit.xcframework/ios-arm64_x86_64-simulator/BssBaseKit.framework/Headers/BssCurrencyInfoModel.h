//
//  BssCurrencyInfoModel.h
//  BssBaseKit
//
//  Created by Korkhonen Viacheslav on 04.02.22.
//  Copyright © 2022 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssMonetaryAmount.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, BssCurrencyInfoModelContributionKey) {
    cocTree
} NS_SWIFT_NAME(BssCurrencyInfoModel.ContributionKey);

@interface BssCurrencyInfoModel : BssJSONModel
@property (nonatomic, strong, nonnull) BssMonetaryAmount * amount;
@end

NS_ASSUME_NONNULL_END
