//
//  UIImage+BssAdditions.h
//  BssBaseKit
//
//  Created by Florian Hager on 30.05.17.
//  Copyright © 2017 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (BssAdditions)

#pragma mark - Barcode
+ (BOOL)BSS_is1DBarcodeType:(NSString *)barcodeType;
+ (BOOL)BSS_is1DMultilineBarcodeType:(NSString *)barcodeType;
+ (BOOL)BSS_is2DBarcodeType:(NSString *)barcodeType;
+ (BOOL)BSS_isSquaredBarcodeType:(NSString *)barcodeType;

+ (UIImage *)BSS_barcodeForSize:(CGSize)maxSize barcodeImage:(UIImage *)barcodeImage barcodeType:(NSString *)barcodeType didAddQuietZone:(void (^)(BOOL didAddQuietZone))didAddQuietZone __attribute__((deprecated("Use UIImage.BSS_barcodeForSize:barcodeImage:barcodeType:resultInfo instead.")));

/*!
 * @discussion Tries to resize the given barcode with best possible scaling factor and quietzones. Quietzones will be considered if possible. If the given barcode is bigger than the given maxSize, the image will be returned unmodified.
 * @param maxSize The given size where the scaled barcode needs to fit in.
 * @param barcodeImage The given barcode image.
 * @param barcodeType The type of the barcode, needed for calculating the correct quietzone. Squared barcodes will be returned as big as possible.
 * @param resultInfo Optional sync block that returns additional infos if the quietzone has been considered and the used scalefactor. Barcodes with scalefactor 1 or where the quietzone has not been considred are possibly not scanable. If barcode image does not fit into given maxSize, scalefactor 0 will be returned.
 * @return The best possible scaled barcode or the given image of not possible to fit in the given maxSize. Quietzone will be considered if possible.
 */
+ (UIImage *)BSS_barcodeForSize:(CGSize)maxSize barcodeImage:(UIImage *)barcodeImage barcodeType:(NSString *)barcodeType resultInfo:(void (^)(BOOL consideredQuietZone, int scaleFactor, UIImage* barcodeImage))resultInfo;


#pragma mark - Tinting
+ (UIImage *)BSS_imageWithColor:(UIColor *)color;
- (UIImage *)BSS_imageWithColor:(UIColor *)color;


#pragma mark - Rotating
- (UIImage *)BSS_imageRotatedByRadians:(CGFloat)radians;
- (UIImage *)BSS_imageRotatedByDegrees:(CGFloat)degrees;

- (UIImage *)BSS_fixOrientation;


#pragma mark - Resizing
- (UIImage *)BSS_resizedImage:(CGSize)newSize;
- (UIImage *)BSS_resizedImage:(CGSize)newSize interpolationQuality:(CGInterpolationQuality)quality;
- (void)BSS_resizedImage:(CGSize)newSize interpolationQuality:(CGInterpolationQuality)quality indexPath:(NSIndexPath *)indexPath completionBlock:(void(^)(UIImage * image, NSIndexPath * givenIndexPath))completionBlock;


#pragma mark - Transforming
- (CGAffineTransform)BSS_transformForOrientation:(CGSize)newSize;


#pragma mark - Cropping
- (UIImage *)BSS_cropImage:(CGRect)rect;

#pragma mark - Compressing
+ (UIImage *)BSS_compressImage:(UIImage *)image;

@end
