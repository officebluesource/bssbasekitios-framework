//
//  BssCardUserBalances.h
//  BssBaseKit
//
//  Created by Hager Florian on 17.06.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssExtensionBalance.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssCardUserBalances : BssJSONModel

@property (strong, nonatomic, nullable) BssExtensionBalance <Optional> * cardUserBalance;
@property (strong, nonatomic, nullable) BssExtensionBalance <Optional> * contributionBalance;

@end

NS_ASSUME_NONNULL_END
