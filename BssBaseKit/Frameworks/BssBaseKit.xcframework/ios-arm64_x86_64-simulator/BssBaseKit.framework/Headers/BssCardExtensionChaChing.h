//
//  BssCardExtensionChaChing.h
//  BssBaseKit
//
//  Created by Hager Florian on 01.07.20.
//  Copyright © 2020 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import "BssJSONModel.h"
#import "BssExtensionBalance.h"
#import "BssExtensionStatus.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssCardExtensionChaChing : BssJSONModel

@property (strong, nonatomic, nullable) BssExtensionBalance <Optional> * balance;
@property (strong, nonatomic, nonnull) BssExtensionStatus * status;
@property (nonatomic, strong, nullable) NSDate <Ignore> * syncDate;

@end

NS_ASSUME_NONNULL_END
