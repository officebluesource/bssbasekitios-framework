//
//  BssLotteryLink.h
//  BssBaseKit
//
//  Created by Hager Florian on 22.02.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssLotteryLink : BssJSONModel

@property (nonatomic, assign) int32_t uniqueId;
@property (strong, nonatomic, nonnull) NSString * label;
@property (strong, nonatomic, nullable) NSString <Optional> * url;
@property (nonatomic, assign) BOOL required;

@end

NS_ASSUME_NONNULL_END
