//
//  BssCPGoodiePreview.h
//  BssBaseKit
//
//  Created by Baumgartner Mario on 28.06.21.
//  Copyright © 2021 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BssJSONModel.h"
#import "BssImage.h"
#import "BssCPRedemption.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssCPGoodiePreview : BssJSONModel

@property (nonatomic, strong, nonnull) NSString * publicId;
@property (nonatomic, strong, nullable) NSString <Optional> * title;
@property (nonatomic, strong, nullable) NSString <Optional> * descr;
@property (nonatomic, strong, nullable) BssImage <Optional> * logo;
@property (nonatomic, strong, nullable) NSDate <Optional> * endDate;
@property (nonatomic, strong, nullable) BssCPRedemption <Optional> * redemption;

@end

NS_ASSUME_NONNULL_END
