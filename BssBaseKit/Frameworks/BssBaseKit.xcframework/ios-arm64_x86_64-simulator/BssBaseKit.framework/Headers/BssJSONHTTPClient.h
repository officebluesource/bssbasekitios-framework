//
//  BssJSONModelHTTPClient.h
//  BssJSONModel
//

#import "BssJSONModel.h"

extern NSString *const kBssHTTPMethodGET DEPRECATED_ATTRIBUTE;
extern NSString *const kBssHTTPMethodPOST DEPRECATED_ATTRIBUTE;
extern NSString *const kBssContentTypeAutomatic DEPRECATED_ATTRIBUTE;
extern NSString *const kBssContentTypeJSON DEPRECATED_ATTRIBUTE;
extern NSString *const kBssContentTypeWWWEncoded DEPRECATED_ATTRIBUTE;

typedef void (^BssJSONObjectBlock)(id json, BssJSONModelError *err) DEPRECATED_ATTRIBUTE;

DEPRECATED_ATTRIBUTE
@interface BssJSONHTTPClient : NSObject

+ (NSMutableDictionary *)requestHeaders DEPRECATED_ATTRIBUTE;
+ (void)setDefaultTextEncoding:(NSStringEncoding)encoding DEPRECATED_ATTRIBUTE;
+ (void)setCachingPolicy:(NSURLRequestCachePolicy)policy DEPRECATED_ATTRIBUTE;
+ (void)setTimeoutInSeconds:(int)seconds DEPRECATED_ATTRIBUTE;
+ (void)setRequestContentType:(NSString *)contentTypeString DEPRECATED_ATTRIBUTE;
+ (void)getJSONFromURLWithString:(NSString *)urlString completion:(BssJSONObjectBlock)completeBlock DEPRECATED_ATTRIBUTE;
+ (void)getJSONFromURLWithString:(NSString *)urlString params:(NSDictionary *)params completion:(BssJSONObjectBlock)completeBlock DEPRECATED_ATTRIBUTE;
+ (void)BssJSONFromURLWithString:(NSString *)urlString method:(NSString *)method params:(NSDictionary *)params orBodyString:(NSString *)bodyString completion:(BssJSONObjectBlock)completeBlock DEPRECATED_ATTRIBUTE;
+ (void)BssJSONFromURLWithString:(NSString *)urlString method:(NSString *)method params:(NSDictionary *)params orBodyString:(NSString *)bodyString headers:(NSDictionary *)headers completion:(BssJSONObjectBlock)completeBlock DEPRECATED_ATTRIBUTE;
+ (void)BssJSONFromURLWithString:(NSString *)urlString method:(NSString *)method params:(NSDictionary *)params orBodyData:(NSData *)bodyData headers:(NSDictionary *)headers completion:(BssJSONObjectBlock)completeBlock DEPRECATED_ATTRIBUTE;
+ (void)postJSONFromURLWithString:(NSString *)urlString params:(NSDictionary *)params completion:(BssJSONObjectBlock)completeBlock DEPRECATED_ATTRIBUTE;
+ (void)postJSONFromURLWithString:(NSString *)urlString bodyString:(NSString *)bodyString completion:(BssJSONObjectBlock)completeBlock DEPRECATED_ATTRIBUTE;
+ (void)postJSONFromURLWithString:(NSString *)urlString bodyData:(NSData *)bodyData completion:(BssJSONObjectBlock)completeBlock DEPRECATED_ATTRIBUTE;

@end
