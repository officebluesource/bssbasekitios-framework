//
//  BssCardExtensions.h
//  BssBaseKit
//
//  Created by Hager Florian on 01.07.20.
//  Copyright © 2020 Bluesource - mobile solutions gbmh. All rights reserved.
//

#import "BssJSONModel.h"
#import "BssCardExtensionChaChing.h"

NS_ASSUME_NONNULL_BEGIN

@interface BssCardExtensions : BssJSONModel

@property (strong, nonatomic, nullable) BssCardExtensionChaChing <Optional> * chaching;

@end

NS_ASSUME_NONNULL_END
